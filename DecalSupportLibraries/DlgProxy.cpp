// DlgProxy.cpp : implementation file
//

#include "stdafx.h"
#include "DecalSupportLibraries.h"
#include "DlgProxy.h"
#include "DecalSupportLibrariesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CDecalSupportLibrariesDlgAutoProxy

IMPLEMENT_DYNCREATE(CDecalSupportLibrariesDlgAutoProxy, CCmdTarget)

CDecalSupportLibrariesDlgAutoProxy::CDecalSupportLibrariesDlgAutoProxy()
{
	EnableAutomation();
	
	// To keep the application running as long as an automation 
	//	object is active, the constructor calls AfxOleLockApp.
	AfxOleLockApp();

	// Get access to the dialog through the application's
	//  main window pointer.  Set the proxy's internal pointer
	//  to point to the dialog, and set the dialog's back pointer to
	//  this proxy.
	ASSERT (AfxGetApp()->m_pMainWnd != NULL);
	ASSERT_VALID (AfxGetApp()->m_pMainWnd);
	ASSERT_KINDOF(CDecalSupportLibrariesDlg, AfxGetApp()->m_pMainWnd);
	m_pDialog = reinterpret_cast<CDecalSupportLibrariesDlg*>(AfxGetApp()->m_pMainWnd);
	m_pDialog->m_pAutoProxy = this;
}

CDecalSupportLibrariesDlgAutoProxy::~CDecalSupportLibrariesDlgAutoProxy()
{
	// To terminate the application when all objects created with
	// 	with automation, the destructor calls AfxOleUnlockApp.
	//  Among other things, this will destroy the main dialog
	if (m_pDialog != NULL)
		m_pDialog->m_pAutoProxy = NULL;
	AfxOleUnlockApp();
}

void CDecalSupportLibrariesDlgAutoProxy::OnFinalRelease()
{
	// When the last reference for an automation object is released
	// OnFinalRelease is called.  The base class will automatically
	// deletes the object.  Add additional cleanup required for your
	// object before calling the base class.

	CCmdTarget::OnFinalRelease();
}

BEGIN_MESSAGE_MAP(CDecalSupportLibrariesDlgAutoProxy, CCmdTarget)
END_MESSAGE_MAP()

BEGIN_DISPATCH_MAP(CDecalSupportLibrariesDlgAutoProxy, CCmdTarget)
END_DISPATCH_MAP()

// Note: we add support for IID_IDecalSupportLibraries to support typesafe binding
//  from VBA.  This IID must match the GUID that is attached to the 
//  dispinterface in the .IDL file.

// {586E5AC4-7A88-4CFA-B075-2D7F0E70250D}
static const IID IID_IDecalSupportLibraries =
{ 0x586E5AC4, 0x7A88, 0x4CFA, { 0xB0, 0x75, 0x2D, 0x7F, 0xE, 0x70, 0x25, 0xD } };

BEGIN_INTERFACE_MAP(CDecalSupportLibrariesDlgAutoProxy, CCmdTarget)
	INTERFACE_PART(CDecalSupportLibrariesDlgAutoProxy, IID_IDecalSupportLibraries, Dispatch)
END_INTERFACE_MAP()

// The IMPLEMENT_OLECREATE2 macro is defined in StdAfx.h of this project
// {29FC66D6-DFC1-45D0-87AD-52D44949DCBC}
IMPLEMENT_OLECREATE2(CDecalSupportLibrariesDlgAutoProxy, "DecalSupportLibraries.Application", 0x29fc66d6, 0xdfc1, 0x45d0, 0x87, 0xad, 0x52, 0xd4, 0x49, 0x49, 0xdc, 0xbc)


// CDecalSupportLibrariesDlgAutoProxy message handlers
