// DlgProxy.h: header file
//

#pragma once

class CDecalSupportLibrariesDlg;


// CDecalSupportLibrariesDlgAutoProxy command target

class CDecalSupportLibrariesDlgAutoProxy : public CCmdTarget
{
	DECLARE_DYNCREATE(CDecalSupportLibrariesDlgAutoProxy)

	CDecalSupportLibrariesDlgAutoProxy();           // protected constructor used by dynamic creation

// Attributes
public:
	CDecalSupportLibrariesDlg* m_pDialog;

// Operations
public:

// Overrides
	public:
	virtual void OnFinalRelease();

// Implementation
protected:
	virtual ~CDecalSupportLibrariesDlgAutoProxy();

	// Generated message map functions

	DECLARE_MESSAGE_MAP()
	DECLARE_OLECREATE(CDecalSupportLibrariesDlgAutoProxy)

	// Generated OLE dispatch map functions

	DECLARE_DISPATCH_MAP()
	DECLARE_INTERFACE_MAP()
};

