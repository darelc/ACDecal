// EventAction.h : Declaration of the cEventAction

#ifndef __EVENTACTION_H_
#define __EVENTACTION_H_

#include "resource.h"       // main symbols
#include <DecalInputImpl.h>

/////////////////////////////////////////////////////////////////////////////
// cEventAction
class ATL_NO_VTABLE cEventAction : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cEventAction, &CLSID_EventAction>,
	public IInputActionImpl< cEventAction >
{
public:
	cEventAction()
	{
	}

   long m_nEvent;

   HRESULT onLoad( LPTSTR szData )
   {
      // Attempt to convert it to a number
      return ( ::_stscanf( szData, _T( "%i" ), &m_nEvent ) == 1 ) ? S_OK : E_INVALIDARG;
   }

DECLARE_REGISTRY_RESOURCEID(IDR_EVENTACTION)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cEventAction)
	COM_INTERFACE_ENTRY(IInputAction)
END_COM_MAP()

// IEventAction
public:
   STDMETHOD(Execute)()
   {
      CComVariant v;
      m_pSite->FireEvent( m_nEvent, v );

      return S_OK;
   }
};

#endif //__EVENTACTION_H_
