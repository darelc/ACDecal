// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__F251EED2_D7E2_4CAB_B862_F92CB04AF3C6__INCLUDED_)
#define AFX_STDAFX_H__F251EED2_D7E2_4CAB_B862_F92CB04AF3C6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#define _WIN32_WINDOWS 0x0410
#define _ATL_APARTMENT_THREADED

#pragma warning(disable:4530)

#ifdef NDEBUG
 #ifdef _ATL_DLL
  #undef _ATL_DLL
 #endif
#endif
#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>
#include <winable.h>

#include <map>
#include <list>
#include <vector>
#include <stack>
#include <fstream>
#import <msxml.dll>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__F251EED2_D7E2_4CAB_B862_F92CB04AF3C6__INCLUDED)
