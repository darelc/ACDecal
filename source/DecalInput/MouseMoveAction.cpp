// MouseMoveAction.cpp : Implementation of cMouseMoveAction
#include "stdafx.h"
#include "DecalInput.h"
#include "MouseMoveAction.h"

/////////////////////////////////////////////////////////////////////////////
// cMouseMoveAction

HRESULT cMouseMoveAction::onLoad( LPTSTR szData )
{
   int nSep = ::_tcscspn( szData, _T( "," ) );
   if( nSep == -1 )
      return E_INVALIDARG;

   szData[ nSep ] = _T( '\0' );

   HRESULT hRes = loadValue( szData, m_x );
   if( FAILED( hRes ) )
      return hRes;

   return loadValue( szData + nSep + 1, m_y );
}

HRESULT cMouseMoveAction::loadValue( LPTSTR szData, cValue &val )
{
   if( *szData == _T( '-' ) )
   {
      // This is from right/bottom
      if( ::_stscanf( szData + 1, _T( "%i" ), &val.m_nMagnitude ) != 1 )
         return E_INVALIDARG;

      if( val.m_nMagnitude < 0 || val.m_nMagnitude > 2000 )
         return E_INVALIDARG;

      val.m_eDir = eRightBottom;
      return S_OK;
   }

   int nLength = ::_tcslen( szData );
   if( szData[ nLength - 1 ] == _T( '%' ) )
   {
      szData[ nLength - 1 ] = _T( '\0' );

      // This is from right/bottom
      if( ::_stscanf( szData, _T( "%i" ), &val.m_nMagnitude ) != 1 )
         return E_INVALIDARG;

      if( val.m_nMagnitude < 0 || val.m_nMagnitude > 100 )
         return E_INVALIDARG;

      val.m_eDir = ePercent;
      return S_OK;
   }

   if( ::_stscanf( szData, _T( "%i" ), &val.m_nMagnitude ) != 1 )
      return E_INVALIDARG;

   if( val.m_nMagnitude < 0 || val.m_nMagnitude > 2000 )
      return E_INVALIDARG;

   val.m_eDir = eLeftTop;
   return S_OK;
}

STDMETHODIMP cMouseMoveAction::Execute()
{
   CComPtr< IInputService > pService;
   HRESULT hRes = m_pSite->get_Service( &pService );
   if( FAILED( hRes ) )
      return hRes;

   CComPtr< IDecal > pDecal;
   hRes = pService->get_Decal( &pDecal );
   if( FAILED( hRes ) )
      return hRes;

   long nWidth, nHeight;
   pDecal->get_ScreenSize( &nWidth, &nHeight );

   return m_pSite->MoveMouse( m_x.value( nWidth ), m_y.value( nHeight ) );
}
