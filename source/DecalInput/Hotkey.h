// Hotkey.h : Declaration of the cHotkey

#ifndef __HOTKEY_H_
#define __HOTKEY_H_

#include "resource.h"       // main symbols
#include "DecalInputCP.h"

/////////////////////////////////////////////////////////////////////////////
// cHotkey
class ATL_NO_VTABLE cHotkey : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cHotkey, &CLSID_Hotkey>,
   public IProvideClassInfo2Impl< &CLSID_Hotkey, &DIID_IHotkeyEvents, &LIBID_DecalInput >,
	public IConnectionPointContainerImpl<cHotkey>,
	public IDispatchImpl<IHotkey, &IID_IHotkey, &LIBID_DecalInput>,
	public CProxyIHotkeyEvents< cHotkey >
{
public:
	cHotkey()
      : m_bEnabled( false ),
      m_nVK( -1 )
	{
	}

   void FinalRelease()
   {
      if( m_bEnabled )
         put_Enabled( VARIANT_FALSE );
   }

   bool m_bEnabled;
   _variant_t m_tag;
   long m_nVK;

DECLARE_REGISTRY_RESOURCEID(IDR_HOTKEY)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cHotkey)
	COM_INTERFACE_ENTRY(IHotkey)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(cHotkey)
CONNECTION_POINT_ENTRY(DIID_IHotkeyEvents)
END_CONNECTION_POINT_MAP()

// IHotkey
public:
	STDMETHOD(get_Enabled)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Enabled)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_Key)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Key)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Tag)(/*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(put_Tag)(/*[in]*/ VARIANT newVal);
};

#endif //__HOTKEY_H_
