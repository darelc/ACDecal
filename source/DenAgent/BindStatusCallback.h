#include "resource.h"
#include "downloaderdlg.h"

class CCallback : public IBindStatusCallback  
{
	public:
		// IUnknown methods
		STDMETHODIMP    QueryInterface(REFIID riid,void ** ppv);
		STDMETHODIMP_(ULONG)    AddRef()    { return m_cRef++; }
		STDMETHODIMP_(ULONG)    Release()   { if (--m_cRef == 0) { delete this; return 0; } return m_cRef; }

		// IBindStatusCallback methods
		STDMETHODIMP    OnStartBinding(DWORD grfBSCOption, IBinding* pbinding);
		STDMETHODIMP    GetPriority(LONG* pnPriority);
		STDMETHODIMP    OnLowResource(DWORD dwReserved);
		STDMETHOD(OnProgress)( /* [in] */ ULONG ulProgress,/* [in] */ ULONG ulProgressMax,/* [in] */ ULONG ulStatusCode,/* [in] */ LPCWSTR szStatusText);
		STDMETHODIMP    OnStopBinding(HRESULT hrResult, LPCWSTR szError);
		STDMETHODIMP    GetBindInfo(DWORD* pgrfBINDF, BINDINFO* pbindinfo);
		STDMETHODIMP    OnDataAvailable(DWORD grfBSCF, DWORD dwSize, FORMATETC *pfmtetc, STGMEDIUM* pstgmed);
		STDMETHODIMP    OnObjectAvailable(REFIID riid, IUnknown* punk);

		// constructors/destructors
		CCallback();
		~CCallback();

		// data members
		DWORD				m_cRef;
		IBinding*			m_pbinding;
		IStream*			m_pstm;
		DWORD				m_cbOld;

		cDownloaderDlg* m_pDlg;

		BOOL  m_bUseTimeout;
		CTime m_timeToStop;
};