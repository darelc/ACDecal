//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by DenAgent.rc
//
#define IDE_NOCLIENTEXE                 1
#define IDC_WEBSITE                     2
#define IDE_NOCLIENTVER                 2
#define IDS_VERSIONTEMPLATE             3
#define IDE_NOXMLDOC                    4
#define IDE_NOXMLVER                    5
#define IDE_XMLCLIENTVERSIONMISMATCH    6
#define IDS_UPDATETEXT                  7
#define IDE_UPDATETEXT                  7
#define IDR_TRAYICON                    101
#define IDD_DENAGENT_DIALOG             102
#define IDR_DENAGENT                    103
#define IDR_MAINFRAME                   128
#define IDR_POPUPS                      129
#define IDD_ADDREMOVE                   130
#define IDD_DOWNLOAD                    131
#define IDD_CHANGEDIR                   134
#define IDD_OPTIONS                     135
#define IDR_VERSION_STATES              136
#define IDB_IMAGES                      141
#define IDB_BITMAP1                     142
#define IDD_DOWNLOADER                  143
#define IDB_GROUPS                      145
#define IDC_CURSOR1                     147
#define IDD_EXPORT                      152
#define IDC_PLUGINS                     1001
#define IDC_REFRESH                     1002
#define IDC_CHANGE_DIR                  1005
#define IDC_PLUGINDIR                   1006
#define IDC_INSTALL                     1007
#define IDC_UPGRADE                     1008
#define IDC_REMOVE                      1009
#define IDC_BROWSE                      1009
#define IDC_PROGRESS                    1011
#define IDC_STATUSTEXT                  1012
#define IDC_STOPDOWNLOAD                1013
#define IDC_ADDREMOVE                   1014
#define IDC_CUSTOMSTATUS                1014
#define IDC_UP                          1015
#define IDC_PROGRESSTOTAL               1015
#define IDC_DOWN                        1016
#define IDC_STATUSTOTAL                 1016
#define IDC_REORDER                     1017
#define IDC_UPDATE                      1017
#define IDC_STATUST                     1017
#define IDC_BARALPHA                    1018
#define IDC_OPTIONS                     1018
#define IDC_VIEWALPHA                   1019
#define IDC_NEWURL                      1022
#define IDC_BARALPHA_SPIN               1023
#define IDC_VIEWALPHA_SPIN              1024
#define IDC_EDIT1                       1028
#define IDC_CUSTOM_FONT_EDIT            1028
#define IDC_EDITEXPORT                  1028
#define IDC_BLENDINGSOFTWARE            1030
#define IDC_BLENDINGGDIPLUS             1031
#define IDC_MESSAGES_TEXT               1031
#define IDC_MEMLOCS_TEXT                1032
#define IDC_DECALPLUGINS_TEXT           1033
#define IDC_CHECK_AUTOSTART             1035
#define IDC_BTN_RESET                   1036
#define IDC_VIEWMULTI                   1037
#define IDC_VIEWSINGLE                  1038
#define IDC_DEFAULT_FONT_RADIO          1039
#define IDC_CLIENT_FONT_RADIO           1040
#define IDC_CUSTOM_FONT_RADIO           1041
#define IDC_RADARYES                    1042
#define IDC_RADIO2                      1043
#define IDC_RADARNO                     1043
#define IDC_RADIO1                      1044
#define IDC_TIMESTAMPON                 1044
#define IDC_TIMESTAMPOFF                1045
#define IDC_DELETE                      1050
#define IDC_BUTTON1                     1051
#define IDC_FORMATHELP                  1051
#define IDC_BTNEXPORT                   1051
#define IDC_EXPORT                      1051
#define IDC_COMBO                       1052
#define IDC_FORMATCLR                   1052
#define IDC_FORMATSTR                   1053
#define IDC_CHECK1                      1054
#define IDC_OLDINJECT                   1054
#define IDC_CHKENABLED                  1054
#define IDC_DUALLOG                     1055
#define IDC_CHKLOC                      1055
#define IDC_WINDOWED                    1056
#define IDC_CHKCLSID                    1056
#define IDC_BUTTON2                     1057
#define IDC_PATCHHELP                   1057
#define IDC_NOMOVIES                    1058
#define IDC_NOMOVIES2                   1059
#define IDC_NOLOGOS                     1059
#define IDC_DOCKPOS                     1060
#define ID_SYSTRAY_CONFIGURE            32771
#define ID_SYSTRAY_EXIT                 32772

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        153
#define _APS_NEXT_COMMAND_VALUE         32776
#define _APS_NEXT_CONTROL_VALUE         1061
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
