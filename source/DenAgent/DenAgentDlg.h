// DenAgentDlg.h : header file
//

#if !defined(AFX_DENAGENTDLG_H__2E961411_B570_4DAA_B5F1_89B9714C39EC__INCLUDED_)
#define AFX_DENAGENTDLG_H__2E961411_B570_4DAA_B5F1_89B9714C39EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Decal.h>

/////////////////////////////////////////////////////////////////////////////
// CDenAgentDlg dialog

class CDenAgentDlg : public CDialog
{
// Construction
public:
	CDenAgentDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDenAgentDlg)
	enum { IDD = IDD_DENAGENT_DIALOG };
	CListCtrl	m_wndPlugins;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDenAgentDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

   struct cPlugin
   {
      CLSID m_id;
      LPCTSTR m_group;
      bool m_bEnabled;
      int m_dragimage;

      cPlugin ( REFCLSID clsid, LPCTSTR group, bool bEnabled, int dragimage )
         : m_id ( clsid ),
         m_group ( group ),
         m_bEnabled ( bEnabled ),
         m_dragimage ( dragimage )
      {
      }
   };

   HIMAGELIST m_hDragImage;
   cPlugin *m_pCurrentDrag;

   int getDragInsertBefore ( CPoint &ptClient );

   void updateDecal(_variant_t vCodeBase);
   void loadPluginList ();

   void GetFileInfoBase(LPCTSTR pszFilename, LPCTSTR pszRootElement, LPCTSTR pszVElement, CString &szFileInfo);
   void updateFileInfo();

public:
   bool m_bDoingUpdate;
   bool m_bDoUpdate;

   CImageList m_groups;

   void convertVersion( LPTSTR szVersion, DWORD &dwVersionMajor, DWORD &dwVersionMinor );

   int iReleaseMajor, iReleaseMinor;
   int iBuildMajor, iBuildMinor;

   CComPtr<IDecal> m_pDecal;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDenAgentDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnRefresh();
	afx_msg void OnAddremove();
	afx_msg void OnUpdate();
	afx_msg void OnOptions();
	afx_msg void OnDeleteitemPlugins(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickPlugins(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBegindragPlugins(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedDelete();
	afx_msg void OnBnClickedExport();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DENAGENTDLG_H__2E961411_B570_4DAA_B5F1_89B9714C39EC__INCLUDED_)
