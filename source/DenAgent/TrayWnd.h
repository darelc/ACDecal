#if !defined(AFX_TRAYWND_H__6CE1FB59_8D19_44C1_8064_0AEE1B3AA745__INCLUDED_)
#define AFX_TRAYWND_H__6CE1FB59_8D19_44C1_8064_0AEE1B3AA745__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TrayWnd.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTrayWnd window

class CTrayWnd : public CWnd
{
// Construction
public:
	CTrayWnd();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTrayWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CTrayWnd();
   bool bEnabled;

   CWnd *m_pDialog;

   UINT_PTR m_uiTimer;

   void showDialog();

   void UpdateXMLFiles();

   BOOL OnEnum( HWND hWndLobby );
   static CTrayWnd* s_pWnd;

	// Generated message map functions
protected:
	//{{AFX_MSG(CTrayWnd)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnSystrayConfigure();
	afx_msg void OnSystrayExit();
	afx_msg LRESULT OnTaskbarRestart(WPARAM, LPARAM);
	//}}AFX_MSG

    afx_msg LRESULT OnSysTray(WPARAM, LPARAM);

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRAYWND_H__6CE1FB59_8D19_44C1_8064_0AEE1B3AA745__INCLUDED_)
