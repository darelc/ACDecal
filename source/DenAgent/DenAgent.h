// DenAgent.h : main header file for the DENAGENT application
//

#if !defined(AFX_DENAGENT_H__A4F37F75_E088_4616_9AC5_3B39579BC3BB__INCLUDED_)
#define AFX_DENAGENT_H__A4F37F75_E088_4616_9AC5_3B39579BC3BB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "DenAgent_i.h"

class CTrayWnd;


// the public key to unencrypt xmls
#include "..\include\DecalKey.h"

/////////////////////////////////////////////////////////////////////////////
// CDenAgentApp:
// See DenAgent.cpp for the implementation of this class
//

class CDenAgentApp : public CWinApp
{
public:
	CDenAgentApp();

   // Returns the formatted string of the AC version
   static bool getVersionString ( LPCTSTR szFilename, CString &strVersion );
   static bool getVersionInfo ( LPCTSTR szFilename, int &iReleaseMajor, int &iReleaseMinor, int &iBuildMajor, int &iBuildMinor );
   static bool getACVersionString ( CString &strVersion );
   static bool getCOMObjectDLL ( REFCLSID rclsid, CString &strFilename );

   // decrypts xml file
   static void DecryptXML( const char *szPath, std::string &szXML );

   // Prepends the agent path to a filename
   static bool getAgentPath ( LPCTSTR szFilename, CString &strPath );
   static bool checkXMLVersion ( CString &strClientVersion, CString &strXMLFile, CTrayWnd* pTrayWnd );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDenAgentApp)
	public:
	virtual BOOL InitInstance();
		virtual int ExitInstance();
	//}}AFX_VIRTUAL

   // Implementation
   static void importV1Plugins ();

   // Helper functions
   static bool checkXMLVersions ( CTrayWnd* pTrayWnd );

   struct cXMLBuild
   {
      LPCTSTR XMLFile;
      long build;
   };

   static void getXMLBuilds ( cXMLBuild *pFirst, cXMLBuild *pLast );

	//{{AFX_MSG(CDenAgentApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	BOOL m_bATLInited;
private:
	BOOL InitATL();
	CTrayWnd* m_pTrayWnd;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DENAGENT_H__A4F37F75_E088_4616_9AC5_3B39579BC3BB__INCLUDED_)
