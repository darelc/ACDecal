// ExportDlg.cpp : implementation file
//

#include "stdafx.h"
#include "DenAgent.h"
#include "ExportDlg.h"
#include "DenAgentDlg.h"
#include <strstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// cExportDlg dialog

cExportDlg::cExportDlg(CWnd* pParent /*=NULL*/)
	: CDialog(cExportDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(cExportDlg)

	//}}AFX_DATA_INIT
}

cExportDlg::~cExportDlg()
{
	m_pDecal.Release();
}

void cExportDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(cExportDlg, CDialog)
	//{{AFX_MSG_MAP(cExportDlg)
	ON_BN_CLICKED(IDC_BTNEXPORT, OnBnClickedBtnexport)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// cExportDlg message handlers

BOOL cExportDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	::SendMessage( GetDlgItem( IDC_CHKENABLED )->m_hWnd, BM_SETCHECK, TRUE, 0 );
	::SendMessage( GetDlgItem( IDC_CHKLOC )->m_hWnd, BM_SETCHECK, FALSE, 0 );
	::SendMessage( GetDlgItem( IDC_CHKCLSID )->m_hWnd, BM_SETCHECK, TRUE, 0 );
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

struct cPluginGroup
{
	LPCTSTR m_groupName,
		m_groupKey;
	int m_iconIndex;
};

void cExportDlg::AppendExportText(const char *szNewText)
{
	int dwOldTextLength =::SendMessage(GetDlgItem(IDC_EDITEXPORT)->m_hWnd, WM_GETTEXTLENGTH, 0, 0) + 1;
	int dwNewTextLength = strlen(szNewText) + 1;
	char *szOldText = new char[dwOldTextLength];
	memset(szOldText, 0, dwOldTextLength);
	char *szBuffer = new char[dwOldTextLength + dwNewTextLength];
	memset(szBuffer, 0, dwOldTextLength + dwNewTextLength);
	::SendMessage(GetDlgItem(IDC_EDITEXPORT)->m_hWnd, WM_GETTEXT, dwOldTextLength, (LPARAM) szOldText);
	if(dwOldTextLength > 0)
		sprintf(szBuffer, "%s%s", szOldText, szNewText);
	else
		sprintf(szBuffer, "%s", szNewText);
	::SendMessage(GetDlgItem(IDC_EDITEXPORT)->m_hWnd, WM_SETTEXT, 0, (LPARAM) szBuffer);
	delete[]szBuffer;
	delete[]szOldText;
}

void cExportDlg::OnBnClickedBtnexport()
{
	USES_CONVERSION;
	std::string szDelim = ", ";
	// Set Cursor
	HCURSOR hCurDefault = GetCursor();
	HCURSOR hCurWaiting = LoadCursor(NULL, IDC_WAIT);
	SetCursor(hCurWaiting);

	// Drakier: Get plugins/filters/services/etc from decal
	cPluginGroup _groups[] = {
		{ _T( "Plugins" ), _T( "Plugins" ), 4 },
		{ _T( "Network Filters" ), _T( "NetworkFilters" ), 1 },
		{ _T( "File Filters" ), _T( "FileFilters" ), 1 },
		{ _T( "Services" ), _T( "Services" ), 0 },
		{ _T( "Surrogates" ), _T( "Surrogates" ), 3 },
		{ _T( "Input Actions" ), _T( "InputActions" ), 2 } },
		*_end_groups = _groups + sizeof ( _groups ) / sizeof ( cPluginGroup );

	int nCount = 0;
	std::string szOutput;
	szOutput = "";
	::SendMessage( GetDlgItem( IDC_EDITEXPORT )->m_hWnd,WM_SETTEXT, 0, (LPARAM)szOutput.c_str());

	szOutput += "type, ";
	if ( ::SendMessage( GetDlgItem( IDC_CHKENABLED )->m_hWnd, BM_GETCHECK, 0, 0 ) )
		szOutput += "enabled, ";
	szOutput += "name, version, ";
	if ( ::SendMessage( GetDlgItem( IDC_CHKLOC )->m_hWnd, BM_GETCHECK, 0, 0 ) )
		szOutput += "location, ";
	if ( ::SendMessage( GetDlgItem( IDC_CHKCLSID )->m_hWnd, BM_GETCHECK, 0, 0 ) )
		szOutput += "clsid";
	szOutput += "\r\n";
	AppendExportText(szOutput.c_str());

	// Go through all the groups exporting their sub-lists
	for ( cPluginGroup *i = _groups; i != _end_groups; ++ i )
	{
		CComPtr<IDecalEnum> pEnum;
		HRESULT hRes = m_pDecal->get_Configuration ( _bstr_t ( i->m_groupKey ), GUID_NULL, &pEnum );
		_ASSERTE( SUCCEEDED ( hRes ) );

		// enum through all items in a group
		while ( pEnum->Next () == S_OK )
		{
			szOutput = "";
			CLSID clsid;
			CComBSTR strName;
			CString strDLL = "";
			CString strDLLVersion = "";
			LPOLESTR szCLSID;

			hRes = pEnum->get_ComClass ( &clsid );
			_ASSERTE ( SUCCEEDED ( hRes ) );

			if (hRes == S_OK)
			{
				if ( CDenAgentApp::getCOMObjectDLL ( clsid, strDLL ) )
				{
					if ( !CDenAgentApp::getVersionString ( strDLL, strDLLVersion ) )
						strDLLVersion = _T( "<No Version>" );
				}
				else
					strDLL = _T( "<No DLL>" );
			}

			szOutput += i->m_groupName;

			if ( ::SendMessage( GetDlgItem( IDC_CHKENABLED )->m_hWnd, BM_GETCHECK, 0, 0 ) )
			{
				VARIANT_BOOL bEnabled;
				hRes = pEnum->get_Enabled ( &bEnabled );
				_ASSERTE ( SUCCEEDED ( hRes ) );
				szOutput += szDelim;
				szOutput += bEnabled?"1":"0";
			}

			hRes = pEnum->get_FriendlyName( &strName );
			_ASSERTE ( SUCCEEDED ( hRes ) );
			szOutput += szDelim;
			if (hRes == S_OK)
				szOutput += OLE2T ( strName );
			else
				szOutput += "<No Name>";

			szOutput += szDelim;
			szOutput += strDLLVersion;

			if ( ::SendMessage( GetDlgItem( IDC_CHKLOC )->m_hWnd, BM_GETCHECK, 0, 0 ) )
			{
				szOutput += szDelim;
				szOutput += strDLL;
			}

			if ( ::SendMessage( GetDlgItem( IDC_CHKCLSID )->m_hWnd, BM_GETCHECK, 0, 0 ) )
			{
				szOutput += szDelim;
				if ( StringFromCLSID(clsid, &szCLSID) == S_OK )
				{
					szOutput += OLE2T(szCLSID);
					::CoTaskMemFree ( szCLSID );
				}
				else
					szOutput += "<No CLSID>";
			}
			szOutput += "\r\n";
			AppendExportText(szOutput.c_str());
		}
	}

	// Drakier: Get the OS Version
	szOutput = "\r\nOperating System:\r\n";
	const int BUFSIZE = 80;
	OSVERSIONINFOEX osvi;
	BOOL bOsVersionInfoEx;

	// Try calling GetVersionEx using the OSVERSIONINFOEX structure.
	// If that fails, try using the OSVERSIONINFO structure.
	ZeroMemory(&osvi, sizeof(OSVERSIONINFOEX));
	osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFOEX);

	if( !(bOsVersionInfoEx = GetVersionEx ((OSVERSIONINFO *) &osvi)) )
	{
		osvi.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
		GetVersionEx ( (OSVERSIONINFO *) &osvi);
	}

	switch (osvi.dwPlatformId)
	{
		// Test for the Windows NT product family.
		case VER_PLATFORM_WIN32_NT:

			// Test for the specific product family.
			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
				szOutput += "Microsoft Windows Server 2003 family, ";
			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 1 )
				szOutput += "Microsoft Windows XP ";
			if ( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
				szOutput += "Microsoft Windows 2000 ";
			if ( osvi.dwMajorVersion <= 4 )
				szOutput += "Microsoft Windows NT ";

			// Test for specific product on Windows NT 4.0 SP6 and later.
			if( bOsVersionInfoEx )
			{
				// Test for the workstation type.
				if ( osvi.wProductType == VER_NT_WORKSTATION )
				{
					if( osvi.dwMajorVersion == 4 )
						szOutput += "Workstation 4.0 ";
					else if( osvi.wSuiteMask & VER_SUITE_PERSONAL )
						szOutput += "Home Edition ";
					else
						szOutput += "Professional ";
				}
            
				// Test for the server type.
				else if ( osvi.wProductType == VER_NT_SERVER )
				{
					if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 2 )
					{
						if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
							szOutput += "Datacenter Edition ";
						else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							szOutput += "Enterprise Edition ";
						else if ( osvi.wSuiteMask == VER_SUITE_BLADE )
							szOutput += "Web Edition ";
						else
							szOutput += "Standard Edition ";
					}

					else if( osvi.dwMajorVersion == 5 && osvi.dwMinorVersion == 0 )
					{
						if( osvi.wSuiteMask & VER_SUITE_DATACENTER )
							szOutput += "Datacenter Server ";
						else if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							szOutput += "Advanced Server ";
						else
							szOutput += "Server ";
					}

					else  // Windows NT 4.0 
					{
						if( osvi.wSuiteMask & VER_SUITE_ENTERPRISE )
							szOutput += "Server 4.0, Enterprise Edition ";
						else
							szOutput += "Server 4.0 ";
					}
				}
			}
			else  // Test for specific product on Windows NT 4.0 SP5 and earlier
			{
				HKEY hKey;
				char szProductType[BUFSIZE];
				DWORD dwBufLen=BUFSIZE;
				LONG lRet;

				lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
						"SYSTEM\\CurrentControlSet\\Control\\ProductOptions",
						0, KEY_QUERY_VALUE, &hKey );
				if( lRet == ERROR_SUCCESS )
				{
					lRet = RegQueryValueEx( hKey, "ProductType", NULL, NULL,
						(LPBYTE) szProductType, &dwBufLen);
					if( (lRet == ERROR_SUCCESS) && (dwBufLen <= BUFSIZE) )
					{
						RegCloseKey( hKey );

						if ( lstrcmpi( "WINNT", szProductType) == 0 )
							szOutput += "Workstation ";
						if ( lstrcmpi( "LANMANNT", szProductType) == 0 )
							szOutput += "Server ";
						if ( lstrcmpi( "SERVERNT", szProductType) == 0 )
							szOutput += "Advanced Server ";

						szOutput += (char)osvi.dwMajorVersion;
						szOutput += ".";
						szOutput += (char)osvi.dwMinorVersion;
					}
				}
			}

			// Display service pack (if any) and build number.
			if( osvi.dwMajorVersion == 4 && 
				lstrcmpi( osvi.szCSDVersion, "Service Pack 6" ) == 0 )
			{
				HKEY hKey;
				LONG lRet;

				// Test for SP6 versus SP6a.
				lRet = RegOpenKeyEx( HKEY_LOCAL_MACHINE,
						"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Hotfix\\Q246009",
						0, KEY_QUERY_VALUE, &hKey );
				if( lRet == ERROR_SUCCESS )
				{
					char szBuild[6]; //might make it a bit longer
					memset(szBuild, 0, 6);
					ltoa(osvi.dwBuildNumber & 0xFFFF, szBuild, 10);

					szOutput += "Service Pack 6a (Build ";
					szOutput += szBuild;
					szOutput += ")\r\n";
				}
				else // Windows NT 4.0 prior to SP6a
				{
					char szBuild[6]; //might make it a bit longer
					memset(szBuild, 0, 6);
					ltoa(osvi.dwBuildNumber & 0xFFFF, szBuild, 10);

					szOutput += osvi.szCSDVersion;
					szOutput += " (Build ";
					szOutput += szBuild;
					szOutput += ")\r\n";
				}
				RegCloseKey( hKey );
			}
			else // Windows NT 3.51 and earlier or Windows 2000 and later
			{
				char szBuild[6]; //might make it a bit longer
				memset(szBuild, 0, 6);
				ltoa(osvi.dwBuildNumber & 0xFFFF, szBuild, 10);

				szOutput += osvi.szCSDVersion;
				szOutput += " (Build ";
				szOutput += szBuild;
				szOutput += ")\r\n";
			}
			break;

		// Test for the Windows 95 product family.
		case VER_PLATFORM_WIN32_WINDOWS:
			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 0)
			{
				szOutput += "Microsoft Windows 95 ";
				if ( osvi.szCSDVersion[1] == 'C' || osvi.szCSDVersion[1] == 'B' )
					szOutput += "OSR2 ";
			} 
			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 10)
			{
				szOutput += "Microsoft Windows 98 ";
				if ( osvi.szCSDVersion[1] == 'A' )
					szOutput += "SE ";
			} 
			if (osvi.dwMajorVersion == 4 && osvi.dwMinorVersion == 90)
			{
				szOutput += "Microsoft Windows Millennium Edition\r\n";
			} 
			break;

		case VER_PLATFORM_WIN32s:
			szOutput += "Microsoft Win32s\r\n";
			break;
	}
	AppendExportText(szOutput.c_str());

	// Drakier: Check the Protected Storage
	RegKey key;
	if ( key.Open( HKEY_LOCAL_MACHINE, _T( "SYSTEM\\CurrentControlSet\\Services\\ProtectedStorage" ) ) == ERROR_SUCCESS )
	{
		DWORD dwStartMode;
		szOutput = "\r\n[Protected Storage Service] : ";
		if ( key.QueryDWORDValue( "Start", dwStartMode ) == ERROR_SUCCESS )
		{
			switch (dwStartMode)
			{
				case 2: //automatic
					szOutput += "Automatic\r\n";
					break;
				case 3: // manual
					szOutput += "Manual\r\n";
					break;
				case 4: // disabled
					szOutput += "Disabled\r\n";
					break;
			}
		}
		key.Close();
		AppendExportText(szOutput.c_str());
	}

	// Drakier: Get the Injection Mode
	if( key.Create( HKEY_LOCAL_MACHINE, _T( "SOFTWARE\\Decal" )) == ERROR_SUCCESS )
	{
		DWORD dwViewMode;
		szOutput = "\r\n[Injection Method] : ";
		if( key.QueryDWORDValue( "OldInjection", dwViewMode )== ERROR_SUCCESS )
		{
			if (dwViewMode == 1)
			{
				szOutput += "OLD\r\n";
			}
			else
			{
				szOutput += "NEW\r\n";
			}
		}
		else
			szOutput += "Error Reading Key";
		key.Close();
		AppendExportText(szOutput.c_str());
	}

	// Drakier: Get the Runtime versions (if installed).
	szOutput = "\r\nRuntime Libraries\r\n";
	AppendExportText(szOutput.c_str());
	// Visual Basic 6 Runtimes
	szOutput = "[msvbvm60.dll]\t: ";
	HMODULE hLib = ::LoadLibrary( "msvbvm60.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// msvcp 7.0 Runtime
	szOutput = "[msvcr70.dll]\t: ";
	hLib = ::LoadLibrary( "msvcr70.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// msvcr 7.0 Runtime
	szOutput = "[msvcp70.dll]\t: ";
	hLib = ::LoadLibrary( "msvcp70.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// msvcp 7.1 Runtime
	szOutput = "[msvcr71.dll]\t: ";
	hLib = ::LoadLibrary( "msvcr71.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// msvcr 7.1 Runtime
	szOutput = "[msvcp71.dll]\t: ";
	hLib = ::LoadLibrary( "msvcp71.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// Haz - mfc+atl
	// mfc 7.0 runtime
	szOutput = "[mfc70.dll]\t: ";
	hLib = ::LoadLibrary( "mfc70.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// mfc 7.1 runtime
	szOutput = "[mfc71.dll]\t: ";
	hLib = ::LoadLibrary( "mfc71.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// atl 7.0 runtime
	szOutput = "[atl70.dll]\t\t: ";
	hLib = ::LoadLibrary( "atl70.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// atl 7.1 runtime
	szOutput = "[atl71.dll]\t\t: ";
	hLib = ::LoadLibrary( "atl71.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// Drakier: Get the MSXML Versions
	szOutput = "\r\nMicrosoft XML Libraries\r\n";
	AppendExportText(szOutput.c_str());
	// msxml3
	szOutput = "[msxml3.dll]\t: ";
	hLib = ::LoadLibrary( "msxml3.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// msxml4
	szOutput = "[msxml4.dll]\t: ";
	hLib = ::LoadLibrary( "msxml4.dll" );
	if( hLib == NULL )
		szOutput += "Not Installed\r\n";
	else
	{
		char strDllFilePath[MAX_PATH] = {""};
		CString strDLLVersion = "";
		GetModuleFileName(hLib, strDllFilePath, MAX_PATH);
		if ( !CDenAgentApp::getVersionString ( strDllFilePath, strDLLVersion ) )
			strDLLVersion = _T( "(No Version)" );
		else
		{
			szOutput += "Installed (";
			szOutput += strDLLVersion;
			szOutput += ")\r\n";
		}
	}
	::FreeLibrary(hLib);
	AppendExportText(szOutput.c_str());

	// Drakier: Get the Compat Mode Layers and display them.
	if (key.Open( HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers" ) == ERROR_SUCCESS)
	{
		RegKey hkItem;
		DWORD dwIndex = 0;
		DWORD dwValues = NULL;
		BOOL bTitle = FALSE;
    
		// Enum regkey for AppCompatFlags checking for client.exe
		::RegQueryInfoKey(key, NULL, NULL, NULL, NULL, NULL, NULL, &dwValues, NULL, NULL, NULL, NULL);
		if (dwValues)
		{
			HRESULT retCode = ERROR_SUCCESS;
			for (dwIndex = 0; dwIndex < dwValues; dwIndex++)
			{
				DWORD dwValueNameLen = 255;
				DWORD dwValueDataLen = 10;
				TCHAR szValueName[255];
				BYTE szValueData[10];
				//TCHAR szValueData[10];
				DWORD dwType;

				retCode = ::RegEnumValue ( key, dwIndex, szValueName, &dwValueNameLen, NULL, &dwType, szValueData, &dwValueDataLen );
				if ( retCode == ERROR_SUCCESS)
				{
					if ( strstr(szValueName, "client.exe") != NULL )
					{
						if (!bTitle)
							szOutput = "\r\nApplication Compatibility\r\n";
						bTitle = TRUE;
						szOutput += szValueName;
						szOutput += ": ";
						szOutput += (char *)szValueData;
						szOutput += "\r\n";
					}
				}
			}
			AppendExportText(szOutput.c_str());
		}
	}
	key.Close();

	// Set Cursor back...
	SetCursor(hCurDefault);

	// Display information
	if (::MessageBox(NULL, "Would you like to export the data to the clipboard?", "Export Data...", MB_YESNO | MB_ICONQUESTION) == IDYES )
	{
		if(OpenClipboard())
		{
			DWORD dwTextLength = ::SendMessage( GetDlgItem( IDC_EDITEXPORT )->m_hWnd, WM_GETTEXTLENGTH, 0, 0) + 1;
			char *szCBText = new char[dwTextLength];
			memset(szCBText, 0, dwTextLength);
			::SendMessage( GetDlgItem( IDC_EDITEXPORT )->m_hWnd, WM_GETTEXT, dwTextLength, (LPARAM)szCBText );
			HGLOBAL hData;
			LPVOID pData;

			EmptyClipboard();
			hData = GlobalAlloc(GMEM_DDESHARE|GMEM_MOVEABLE,strlen(szCBText) + 1);
			pData = GlobalLock(hData);
			strcpy((LPSTR)pData, szCBText);
			GlobalUnlock(hData);
			SetClipboardData(CF_TEXT, hData);
			CloseClipboard();
			delete[] szCBText;
		}
		else
			::MessageBox(NULL, "Error opening clipboard!", "Error...", MB_OK | MB_ICONERROR);
	}
}

void cExportDlg::SetDecal(IDecal* pDecal)
{
	m_pDecal = pDecal;
}