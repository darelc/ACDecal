// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__017FA8D0_FE44_43D6_956B_FB615165AF2B__INCLUDED_)
#define AFX_STDAFX_H__017FA8D0_FE44_43D6_956B_FB615165AF2B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define WINVER 0x0410
#define _WIN32_DCOM

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#pragma warning(disable:4530)

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <vector>

#include <atlbase.h>
#include <comdef.h>
#include <atlcrypt.h>

#define _ATL_APARTMENT_THREADED
#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
class CDenAgentModule : public CComModule
{
public:
	LONG Unlock();
	LONG Lock();
	LPCTSTR FindOneOf(LPCTSTR p1, LPCTSTR p2);
	DWORD dwThreadID;
};
extern CDenAgentModule _Module;
#include <atlcom.h>

#import <msxml.dll>
#include <list>
#include <string>

#include "../include/Helpers.h"

extern LONG g_fAbortDownload;

struct FILELIST { CString strFile; CString strLFile; };

//extern FILELIST g_FileList[3];


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__017FA8D0_FE44_43D6_956B_FB615165AF2B__INCLUDED_)
