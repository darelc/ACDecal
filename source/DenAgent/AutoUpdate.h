#ifndef __AUTOUPDATE_H__
#define __AUTOUPDATE_H__

class DownloadAgent {
public:
	static bool downloadFile(std::string remoteFile, std::string localFile);

	DownloadAgent();

	void scheduleDownload(std::string remoteFile, std::string localFile);
	bool runDownloads();

private:
	struct DownloadSlot {
		std::string remoteFile, localFile;
	};

	std::list<DownloadSlot> m_ScheduledDownloads;
};

enum AutoUpdateType {
	AU_TYPE_XML,
	AU_TYPE_DLL,
	AU_TYPE_BETADLL
};

class UpdateRequirement
{
public:
	UpdateRequirement(std::string File, std::string Version):
		sFile(File), sVers(Version)	{ }
	std::string sFile;
	std::string sVers;
};

class AutoUpdateSource {
public:
	AutoUpdateSource(std::string decalPath, AutoUpdateType type, std::string localTarget, std::string remoteSource, std::string requiredVersion);

	bool needsUpdating();
	std::string getSource();
	std::string getDestination();
	AutoUpdateType getType();
	bool isOutDatedVersion(std::string sFile, int nMajor, int nMinor, int nPatch, int nBuild);
	void AddRequirement(UpdateRequirement reqUpdate);

private:
	std::string m_LocalTarget;
	std::string m_RemoteSource;
	std::string m_RequiredVersion;
	std::string m_DecalDir;
	AutoUpdateType m_Type;
	std::vector<UpdateRequirement> m_Requirements;
};

class AutoUpdate {
public:
	AutoUpdate(std::string decalDir);

	bool initialiseFromXML(std::string remoteXML);
	bool needsUpdate();
	bool performUpdate();

private:
	std::string m_LocalXML, m_RemoteXML;
	std::string m_DecalDir;
	std::list<AutoUpdateSource> m_RemoteSources;
};

#endif