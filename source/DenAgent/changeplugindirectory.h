#if !defined(AFX_CCHANGEPLUGINDIRECTORY_H__3AB22538_1DA2_48AB_A4E8_CFD30AD52A75__INCLUDED_)
#define AFX_CCHANGEPLUGINDIRECTORY_H__3AB22538_1DA2_48AB_A4E8_CFD30AD52A75__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// cChangePluginDirectory.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// cChangePluginDirectory dialog

class cChangePluginDirectory : public CDialog
{
// Construction
public:
	cChangePluginDirectory(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(cChangePluginDirectory)
	enum { IDD = IDD_CHANGEDIR };
	CEdit	m_NewUrl;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cChangePluginDirectory)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(cChangePluginDirectory)
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CCHANGEPLUGINDIRECTORY_H__3AB22538_1DA2_48AB_A4E8_CFD30AD52A75__INCLUDED_)
