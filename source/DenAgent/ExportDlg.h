#include "afxwin.h"
#if !defined(AFX_EXPORTDLG_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_)
#define AFX_EXPORTDLG_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <Decal.h>

// ExportDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// cExportDlg dialog

class cExportDlg : public CDialog
{
// Construction
public:
	cExportDlg(CWnd* pParent = NULL);   // standard constructor
	~cExportDlg();

// Dialog Data
	//{{AFX_DATA(cExportDlg)
	enum { IDD = IDD_EXPORT };

  //}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(cExportDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(cExportDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	void SetDecal(IDecal* pDecal);
	void AppendExportText(const char* szNewText);
	CComPtr<IDecal> m_pDecal;
	afx_msg void OnBnClickedBtnexport();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ExportDlg_H__F2DBFC10_6835_494D_9A03_B97D9068BAF9__INCLUDED_)
