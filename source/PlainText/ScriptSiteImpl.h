// ScriptSiteImpl.h
// Declaration of class IActiveScriptSiteImpl

#ifndef __SCRIPTSITEIMPL_H
#define __SCRIPTSITEIMPL_H

class IActiveScriptSiteImpl
: public IActiveScriptSite
{
   class cObject
   {
   public:
      cObject( BSTR szName, IUnknown *pObject )
         : m_strName( szName ),
         m_pObject( pObject )
      {
      }

      _bstr_t m_strName;
      CComPtr< IUnknown > m_pObject;
   };

public:

   typedef std::deque< cObject > cObjectList;
   cObjectList m_objects;

   CComPtr< IActiveScript > m_pScript;

   IActiveScriptSiteImpl()
      : m_pScript( NULL )
   {
   }

   HRESULT createScriptEngine( LPCOLESTR strLanguage );

   // Helper functions
   void addNamedItem( BSTR szName, IUnknown *pObject, DWORD dwFlags = SCRIPTITEM_ISSOURCE | SCRIPTITEM_ISVISIBLE );

   // IActiveScriptSite Methods
   STDMETHOD(GetLCID)(LCID *plcid)
   {
      return E_NOTIMPL;
   }

   STDMETHOD(GetItemInfo)(LPCOLESTR pstrName, DWORD dwReturnMask, IUnknown **ppunkItem, ITypeInfo **ppTypeInfo);
   STDMETHOD(GetDocVersionString)(BSTR *pbstrVersionString)
   {
      return E_NOTIMPL;
   }

   STDMETHOD(OnScriptTerminate)(const VARIANT *pvarResult, const EXCEPINFO *pexcepinfo)
   {
      return S_OK;
   }

   STDMETHOD(OnStateChange)(SCRIPTSTATE ssScriptState)
   {
      if( ssScriptState == SCRIPTSTATE_CLOSED )
      {
         m_objects.clear();
         m_pScript.Release();
      }

      return S_OK;
   }

   STDMETHOD(OnScriptError)(IActiveScriptError *pase);

   STDMETHOD(OnEnterScript)()
   {
      return S_OK;
   }

   STDMETHOD(OnLeaveScript)()
   {
      return S_OK;
   }
};

#endif

