// ScriptView.h : Declaration of the CScriptView

//
// DEVELOPER COMMENTS (elphkotm)
// This is an import of the February Decal PlainText class, called
// cScriptView, I intentionally use an upper case C bye the way.
//

#ifndef __SCRIPTVIEW_H_
#define __SCRIPTVIEW_H_

#include "resource.h"       // main symbols
#include "ScriptPlugin.h"
#include <DecalImpl.h>
#include "ScriptSiteImpl.h"
#include "../Inject/SinkImpl.h"
#include "PlainTextCP.h"

#define VIEW_ID   1

/////////////////////////////////////////////////////////////////////////////
// CScriptView
class ATL_NO_VTABLE CScriptView : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CScriptView, &CLSID_ScriptView>,
   public IActiveScriptSiteImpl,
   public IProvideClassInfo2Impl< &CLSID_ScriptView, &IID_IScriptView, &LIBID_PlainText >,
   public IViewEventsImpl<VIEW_ID, CScriptView>,
	public IDispatchImpl<IScriptView, &IID_IScriptView, &LIBID_PlainText>,
	public IConnectionPointContainerImpl<CScriptView>,
	public CProxyIScriptViewEvents< CScriptView >
{
public:
	CScriptView()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_SCRIPTVIEW)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CScriptView)
   COM_INTERFACE_ENTRY(IActiveScriptSite)
	COM_INTERFACE_ENTRY(IScriptView)
	COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(CScriptView)
CONNECTION_POINT_ENTRY(DIID_IScriptViewEvents)
END_CONNECTION_POINT_MAP()

BEGIN_SINK_MAP(CScriptView)
   SINK_ENTRY_EX( VIEW_ID, DIID_IViewEvents, DISPID_ACTIVATE, onViewActivate )
   SINK_ENTRY_EX( VIEW_ID, DIID_IViewEvents, DISPID_DEACTIVATE, onViewDeactivate )
END_SINK_MAP()

// IScriptView
public:
	void loadView( cScriptPlugin *pPlugin, MSXML::IXMLDOMElementPtr &pView, LPDISPATCH *ppNewView );
	void destroy();

   CComPtr< IView > m_pView;
	CComPtr< IPluginSite > m_pOldSite;

	cScriptPlugin *m_pPlugin;
	_bstr_t m_strTemplate;

   // Method Calls
   void __stdcall onViewActivate()
   {
      Fire_Activate();
   }

   void __stdcall onViewDeactivate()
   {
      Fire_Deactivate();
   }
};

#endif //__SCRIPTVIEW_H_
