// PageLayout.h : Declaration of the cPageLayout

#ifndef __PAGELAYOUT_H_
#define __PAGELAYOUT_H_

#include "resource.h"       // main symbols

#include "ControlImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cPageLayout
class ATL_NO_VTABLE cPageLayout : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cPageLayout, &CLSID_PageLayout>,
	public ILayoutImpl< cPageLayout, IPageLayout, &IID_IPageLayoutDisp, &LIBID_DecalControls >,
   public ILayerImpl< cPageLayout >,
   public IProvideClassInfo2Impl< &CLSID_PageLayout, &DIID_IControlEvents, &LIBID_DecalControls >,
   public IConnectionPointContainerImpl<cPageLayout>,
   public CProxyIControlEvents< cPageLayout >
{
public:
	cPageLayout()
      : m_nActive( -1 )
	{
	}

   // Override functions
   void onChildDestroy( long nID );
   void onSchemaLoad( IView *pView, MSXML::IXMLDOMElementPtr &pElement );

   typedef std::deque< long > cChildIDList;
   cChildIDList m_pages;

   long m_nActive;

DECLARE_REGISTRY_RESOURCEID(IDR_PAGELAYOUT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPageLayout)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IPageLayout)
   COM_INTERFACE_ENTRY(IPageLayoutDisp)
   COM_INTERFACE_ENTRY(ILayout)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
   COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cPageLayout)
CONNECTION_POINT_ENTRY(DIID_IControlEvents)
END_CONNECTION_POINT_MAP()

public:
   // IPageLayout Methods
	STDMETHOD(get_Count)(/*[out, retval]*/ long *pVal);
	STDMETHOD(CreatePage)(ILayer *pChild);
	STDMETHOD(get_Active)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Active)(/*[in]*/ long newVal);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
};

#endif //__PAGELAYOUT_H_
