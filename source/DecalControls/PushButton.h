// PushButton.h : Declaration of the cPushButton

#ifndef __PUSHBUTTON_H_
#define __PUSHBUTTON_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cPushButton
class ATL_NO_VTABLE cPushButton : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cPushButton, &CLSID_PushButton>,
	public IControlImpl< cPushButton, IPushButton, &IID_IPushButton, &LIBID_DecalControls >,
   public ILayerImpl< cPushButton >,
   public ILayerRenderImpl,
   public ILayerSchema,
   public ILayerMouseImpl,
   public IProvideClassInfo2Impl< &CLSID_PushButton, &DIID_ICommandEvents, &LIBID_DecalControls >,
   public IConnectionPointContainerImpl<cPushButton>,
   public CProxyICommandEvents< cPushButton >
{
public:
	cPushButton(); 

   VARIANT_BOOL m_bPressed,
      m_bMouseIn;

   _bstr_t m_strText;
   CComPtr< IFontCache > m_pFont;
   CComPtr< IImageCache > m_pBackground;
   POINT m_ptText;

   long m_nFaceColor,
      m_nTextColor;
   BOOL m_UseFaceColor;		// GKusnick: Render 3D outline.
   bool m_bAA;

   void onCreate();

DECLARE_REGISTRY_RESOURCEID(IDR_PUSHBUTTON)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPushButton)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IPushButton)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cPushButton)
CONNECTION_POINT_ENTRY(DIID_ICommandEvents)
END_CONNECTION_POINT_MAP()

public:
   // IPushButton Methods
	STDMETHOD(get_TextColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);
	STDMETHOD(get_FaceColor)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_FaceColor)(/*[in]*/ long newVal);
	STDMETHOD(get_Text)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Text)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Font)(/*[out, retval]*/ IFontCacheDisp * *pVal);
	STDMETHOD(putref_Font)(/*[in]*/ IFontCacheDisp * newVal);
	STDMETHOD(get_Image)(/*[out, retval]*/ IImageCacheDisp * *pVal);
	STDMETHOD(putref_Image)(/*[in]*/ IImageCacheDisp * newVal);

   // ILayerMouse Methods
	STDMETHOD(MouseEnter)(struct MouseState *);
	STDMETHOD(MouseExit)(struct MouseState *);
	STDMETHOD(MouseDown)(struct MouseState *);
	STDMETHOD(MouseUp)(struct MouseState *);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)( IView *pView, IUnknown *pSchema );
};

#endif //__PUSHBUTTON_H_
