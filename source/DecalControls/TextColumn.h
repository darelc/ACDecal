// TextColumn.h : Declaration of the cTextColumn

#ifndef __TEXTCOLUMN_H_
#define __TEXTCOLUMN_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cTextColumn
class ATL_NO_VTABLE cTextColumn : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cTextColumn, &CLSID_TextColumn>,
	public IListColumn
{
public:
	cTextColumn()
      : m_nFixedWidth( -1 )
	{
	}

   CComPtr< IList > m_pList;
   CComPtr< IPluginSite > m_pSite;
	
   long m_nFixedWidth;
   long m_nRowHeight;
   long m_nFontSize;
   long m_nTextX;
   long m_nTextY;
   eFontJustify m_eJustify;		// GKusnick: Justification option.

DECLARE_REGISTRY_RESOURCEID(IDR_TEXTCOLUMN)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cTextColumn)
	COM_INTERFACE_ENTRY(IListColumn)
END_COM_MAP()

// IListColumn
public:
	STDMETHOD(Activate)(LPPOINT ptCell);
	STDMETHOD(get_Height)(/*[out, retval]*/ long *pVal);
	STDMETHOD(SchemaLoad)(IUnknown *pSchema);
   STDMETHOD(Initialize)(/*[in]*/ IList * newVal, IPluginSite *);
	STDMETHOD(get_DataColumns)(/*[out, retval]*/ long *pVal);
	STDMETHOD(Render)(ICanvas *, LPPOINT ptCell, long nColor);
	STDMETHOD(get_Width)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_FixedWidth)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Width)(/*[in]*/ long newVal);
};

#endif //__TEXTCOLUMN_H_
