// Progress.h : Declaration of the cProgress

#ifndef __PROGRESS_H_
#define __PROGRESS_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cProgress
class ATL_NO_VTABLE cProgress : 
	public CComObjectRootEx< CComSingleThreadModel >,
	public CComCoClass< cProgress, &CLSID_Progress >,
	public IConnectionPointContainerImpl< cProgress >,
	public IControlImpl< cProgress, IProgress, &IID_IProgress, &LIBID_DecalControls >,
	public ILayerImpl< cProgress >,
	public ILayerRenderImpl,
	public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_Progress, &DIID_IControlEvents, &LIBID_DecalControls >,
	public CProxyIControlEvents< cProgress >
{
public:
	cProgress()
	{
		m_nFaceColor = RGB(140, 80, 30);
		m_nFillColor = RGB(220, 140, 60);
		m_nProgress = 50;
		m_nTextColor = RGB(240, 240, 120);
		m_nBorderColor = RGB(240, 240, 120);
		m_nBorderWidth = 1;
		m_nMaxProgress = 100;
		m_strPost = "%";
		m_nDrawText = true;
		m_nAlignment = 1; // center
	}

DECLARE_REGISTRY_RESOURCEID(IDR_PROGRESS)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cProgress)
	COM_INTERFACE_ENTRY(ILayerRender)
	COM_INTERFACE_ENTRY(ILayerSchema)
	COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IControl)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IProgress)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cProgress)
	CONNECTION_POINT_ENTRY(DIID_IControlEvents)
END_CONNECTION_POINT_MAP()

public:
	STDMETHOD(put_BorderColor)(/*[in]*/ long newVal);
	STDMETHOD(put_BorderWidth)(/*[in]*/ long newVal);
	STDMETHOD(put_PreText)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_DrawText)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(put_Alignment)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_PostText)(/*[in]*/ BSTR newVal);
	STDMETHOD(put_MaxValue)(/*[in]*/ long newVal);
	STDMETHOD(put_TextColor)(/*[in]*/ long newVal);
	STDMETHOD(put_FillColor)(/*[in]*/ long newVal);
	STDMETHOD(put_FaceColor)(/*[in]*/ long newVal);
	STDMETHOD(get_Value)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Value)(/*[in]*/ long newVal);

	// ILayerSchema Methods
	STDMETHOD(SchemaLoad)(IView *pView, IUnknown *pSchema);

	// ILayerRender Methods
	STDMETHOD(Reformat)();
	STDMETHOD(Render)(ICanvas *pCanvas);

	long m_nProgress, m_nFaceColor, m_nTextColor, m_nFillColor,
		m_nMaxProgress, m_nBorderColor, m_nBorderWidth, m_nDrawText;
	std::string m_strPost, m_strPre;
	POINT m_ptText;
	_bstr_t m_bstrText;
	int m_nAlignment; // 0 = left, 1 = center, 2 = right
   CComPtr< IFontCache > m_pFont;
};

#endif //__PROGRESS_H_
