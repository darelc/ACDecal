// DecalControls.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f DecalControlsps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "DecalControls.h"

#include "Scroller.h"
#include "List.h"

#include "TextColumn.h"
#include "IconColumn.h"
#include "CheckColumn.h"
#include "Notebook.h"

#include "DecalControls_i.c"
#include "Edit.h"
#include "Choice.h"
#include "FixedLayout.h"
#include "BorderLayout.h"
#include "PageLayout.h"
#include "PushButton.h"
#include "Static.h"
#include "Checkbox.h"
#include "DerethMap.h"
#include "Slider.h"
#include "Progress.h"

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
   OBJECT_ENTRY(CLSID_Scroller, cScroller)
   OBJECT_ENTRY(CLSID_List, cList)
   OBJECT_ENTRY(CLSID_TextColumn, cTextColumn)
   OBJECT_ENTRY(CLSID_IconColumn, cIconColumn)
   OBJECT_ENTRY(CLSID_CheckColumn, cCheckColumn)
   OBJECT_ENTRY(CLSID_Notebook, cNotebook)
   OBJECT_ENTRY(CLSID_Edit, cEdit)
   OBJECT_ENTRY(CLSID_Choice, cChoice)
   OBJECT_ENTRY(CLSID_FixedLayout, cFixedLayout)
   OBJECT_ENTRY(CLSID_BorderLayout, cBorderLayout)
   OBJECT_ENTRY(CLSID_PageLayout, cPageLayout)
   OBJECT_ENTRY(CLSID_PushButton, cPushButton)
   OBJECT_ENTRY(CLSID_StaticText, cStatic)
   OBJECT_ENTRY(CLSID_Checkbox, cCheckbox)
   OBJECT_ENTRY(CLSID_DerethMap, cDerethMap)
   OBJECT_ENTRY(CLSID_Slider, cSlider)
   OBJECT_ENTRY(CLSID_Progress, cProgress)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID /*lpReserved*/)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, &LIBID_DecalControls);
        DisableThreadLibraryCalls(hInstance);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
        _Module.Term();
    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}


