// Notebook.h : Declaration of the cNotebook

#ifndef __NOTEBOOK_H_
#define __NOTEBOOK_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cNotebook
class ATL_NO_VTABLE cNotebook : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cNotebook, &CLSID_Notebook>,
	public IConnectionPointContainerImpl<cNotebook>,
   public ILayerImpl< cNotebook >,
   public ILayerRenderImpl,
   public ILayerMouseImpl,
   public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_Notebook, &DIID_INotebookEvents, &LIBID_DecalControls >,
	public IControlImpl< cNotebook, INotebook, &IID_INotebook, &LIBID_DecalControls >,
	public CProxyINotebookEvents< cNotebook >
{
public:
	cNotebook();

   class cPage
   {
   public:
      _bstr_t m_strName;
      long m_nPageID;

      long m_nLeft, m_nRight;
   };

   typedef std::vector< cPage > cPageList;
   cPageList m_pages;

   CComPtr< IImageCache > m_pActive;
   CComPtr< IImageCache > m_pInactive;
   CComPtr< IFontCache > m_pFont;

   long m_nNextPageID,
      m_nActiveTab,
      m_nCaptureTab;
   bool m_bHitCapture;

   long hitTest( MouseState *pMS );
   void positionActiveTab();

   void onCreate();
   void onDestroy();

DECLARE_REGISTRY_RESOURCEID(IDR_NOTEBOOK)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cNotebook)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
	COM_INTERFACE_ENTRY(INotebook)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cNotebook)
CONNECTION_POINT_ENTRY(DIID_INotebookEvents)
END_CONNECTION_POINT_MAP()

public:
   // INotebook Methods
	STDMETHOD(AddPage)(BSTR strText, IControl *pClient);
	STDMETHOD(get_ActiveTab)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_ActiveTab)(/*[in]*/ long newVal);
	STDMETHOD(get_PageText)(long nIndex, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_PageText)(long nIndex, /*[in]*/ BSTR newVal);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)( ICanvas *pCanvas );

   // ILayerMouse Methods
   STDMETHOD(MouseDown)( MouseState *pMS );
   STDMETHOD(MouseUp)( MouseState *pMS );
   STDMETHOD(MouseMove)( MouseState *pMS );

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *, IUnknown *pSchema);
};

#endif //__NOTEBOOK_H_
