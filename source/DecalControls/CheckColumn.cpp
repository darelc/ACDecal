// CheckColumn.cpp : Implementation of cCheckColumn
#include "stdafx.h"
#include "DecalControls.h"
#include "CheckColumn.h"

/////////////////////////////////////////////////////////////////////////////
// cCheckColumn

STDMETHODIMP cCheckColumn::get_FixedWidth(VARIANT_BOOL *pVal)
{
   *pVal = VARIANT_TRUE;

	return S_OK;
}

STDMETHODIMP cCheckColumn::get_Width(long *pVal)
{
   *pVal = 20;

	return S_OK;
}

STDMETHODIMP cCheckColumn::put_Width(long newVal)
{
//	m_Width = newVal;

	return S_OK;
}

STDMETHODIMP cCheckColumn::Render(ICanvas *pCanvas, LPPOINT ptCell, long nColor)
{
	_variant_t vCheck, vHidden;
	bool bHidden, bCheck;

	m_pList->get_Data( ptCell->x, ptCell->y, 0, &vCheck );
	bCheck = vCheck.vt == VT_NULL ? false : static_cast<bool>(vCheck);

	m_pList->get_Data( ptCell->x, ptCell->y, 1, &vHidden );
	bHidden = vHidden.vt == VT_NULL ? false : static_cast<bool>(vHidden);

	// Converted nicely now ...
	// Find our checked and unchecked images
	CComPtr< IIconCache > pIcons;

	SIZE sz = { 13, 13 };
	m_pSite->GetIconCache( &sz, &pIcons );

	POINT pt = { 3, 3 };
	if( !bHidden )
		pIcons->DrawIcon( &pt, bCheck ? 0x0600128B : 0x0600128D, 0, pCanvas );

	return S_OK;
}

STDMETHODIMP cCheckColumn::get_DataColumns(long *pVal)
{
   *pVal = 2;

	return S_OK;
}

STDMETHODIMP cCheckColumn::Initialize(IList *newVal, IPluginSite *pSite)
{
   m_pList = newVal;
   m_pSite = pSite;

	return S_OK;
}

STDMETHODIMP cCheckColumn::SchemaLoad(IUnknown *pSchema)
{
	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP cCheckColumn::get_Height(long *pVal)
{
   *pVal = 20;

	return S_OK;
}

STDMETHODIMP cCheckColumn::Activate(LPPOINT ptCell)
{
	_variant_t vCheck, vHidden;

	bool bHidden, bCheck;

	m_pList->get_Data( ptCell->x, ptCell->y, 0, &vCheck );
	bCheck = vCheck.vt == VT_NULL ? false : static_cast<bool>(vCheck);

	m_pList->get_Data( ptCell->x, ptCell->y, 1, &vHidden );
	bHidden = vHidden.vt == VT_NULL ? false : static_cast<bool>(vHidden);

	if( !bHidden )
		m_pList->put_Data( ptCell->x, ptCell->y, 0, &_variant_t( !bCheck ) );

	return S_OK;
}

STDMETHODIMP cCheckColumn::get_Color(long nRow, long *pVal)
{
	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP cCheckColumn::put_Color(long nRow, long newVal)
{
	// TODO: Add your implementation code here

	return S_OK;
}
