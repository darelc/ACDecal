// DerethMap.h: Definition of the cDerethMap class
//
//////////////////////////////////////////////////////////////////////
#ifndef __DERETHMAP_H_
#define __DERETHMAP_H_

#if !defined(AFX_DERETHMAP_H__D110354A_8D4A_485A_9379_961C3A6E445D__INCLUDED_)
#define AFX_DERETHMAP_H__D110354A_8D4A_485A_9379_961C3A6E445D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

#define MAPSIZE_X 245
#define MAPSIZE_Y 245
#define MAPBORDER_X 6 
#define MAPBORDER_Y  11

/////////////////////////////////////////////////////////////////////////////
// cDerethMap

class ATL_NO_VTABLE cDerethMap : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cDerethMap,&CLSID_DerethMap>,
	public IControlImpl< cDerethMap, IDerethMap, &IID_IDerethMap, &LIBID_DecalControls >,
   public ILayerImpl< cDerethMap >,
   public ILayerRenderImpl,
   public ILayerSchema,
   public ILayerMouseImpl,
   public IProvideClassInfo2Impl< &CLSID_DerethMap, &DIID_ICommandEvents, &LIBID_DecalControls >,
   public IConnectionPointContainerImpl<cDerethMap>,
   public CProxyICommandEvents< cDerethMap >
{
public:
	cDerethMap();
	~cDerethMap();

	CComPtr< IImageCache > m_pBackground;
	CComPtr< IImageCache > m_pObjectImage;
	CComPtr< IFontCache > m_pFont;

	RECT m_rcMapSrc;

DECLARE_REGISTRY_RESOURCEID(IDR_DerethMap)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDerethMap)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IDerethMap)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
   COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cDerethMap)
	CONNECTION_POINT_ENTRY(DIID_ICommandEvents)
END_CONNECTION_POINT_MAP()


   void onCreate();
   void onDestroy();
   void checkFont();

public:

	// ILayerMouse Methods
	STDMETHOD(MouseEnter)(struct MouseState *);
	STDMETHOD(MouseExit)(struct MouseState *);
	STDMETHOD(MouseDown)(struct MouseState *);
	STDMETHOD(MouseUp)(struct MouseState *);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)( IView *pView, IUnknown *pSchema );
};

#endif // !defined(AFX_DERETHMAP_H__D110354A_8D4A_485A_9379_961C3A6E445D__INCLUDED_)
#endif
