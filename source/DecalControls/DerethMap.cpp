// DerethMap.cpp : Implementation of CDecalControlsApp and DLL registration.

#include "stdafx.h"
#include "DecalControls.h"
#include "DerethMap.h"

/////////////////////////////////////////////////////////////////////////////
//
cDerethMap::cDerethMap()
{
	m_rcMapSrc.left = m_rcMapSrc.top  = 100;
	m_rcMapSrc.right = m_rcMapSrc.bottom = 200;
}

cDerethMap::~cDerethMap()
{
}

void cDerethMap::onCreate()
{
	CComPtr< IButton > pRollup;

   HRESULT hRes = ::CoCreateInstance( CLSID_Button, NULL, CLSCTX_INPROC_SERVER, IID_IButton, reinterpret_cast< void ** >( &pRollup ) );

   _ASSERTE( SUCCEEDED( hRes ) );

   CComPtr< ILayer > pBtnLayer;
   pRollup->QueryInterface( &pBtnLayer );

   LayerParams lp = { 1, { 180 - 22, 0, 180, 31 }, eRenderTransparent };
   m_pSite->CreateChild( &lp, pBtnLayer );

   pRollup->put_Matte( RGB( 0, 0, 0 ) );
   pRollup->SetImages( 0, 0x06001127, 0x06001128 );
}

void cDerethMap::onDestroy()
{

}

STDMETHODIMP cDerethMap::SchemaLoad( IView *pView, IUnknown *pSchema )
{
	CComPtr< IPluginSite > pPlugin;
	m_pSite->get_PluginSite( &pPlugin );
	pPlugin->LoadImageSchema( pSchema, &m_pBackground );

	pPlugin->LoadBitmapFile( _bstr_t( "MapObject.bmp" ), &m_pObjectImage );
	BSTR bstrFontName;
	pPlugin->get_FontName(&bstrFontName);
	pPlugin->CreateFont( bstrFontName /*_bstr_t( _T( "Times New Roman" ) )*/, 14, 0, &m_pFont );
   
	return S_OK;
}

void cDerethMap::checkFont()
{
   if( m_pFont.p == NULL )
   {
      CComPtr< IPluginSite > pPlugin;
      m_pSite->get_PluginSite( &pPlugin );

	  BSTR bstrFontName;
	  pPlugin->get_FontName(&bstrFontName);
      pPlugin->CreateFont( bstrFontName /*_bstr_t( _T( "Times New Roman" ) )*/, 14, 0, &m_pFont );
   }
}

STDMETHODIMP cDerethMap::MouseEnter(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP cDerethMap::MouseExit(struct MouseState *)
{
	return S_OK;
}

STDMETHODIMP cDerethMap::MouseDown(struct MouseState *pms)
{
	m_rcMapSrc.left += 5;
	m_rcMapSrc.right = 600;
	m_pSite->Invalidate( );
	return S_OK;
}

STDMETHODIMP cDerethMap::MouseUp(struct MouseState *)
{
	m_rcMapSrc.left += 5;
	m_rcMapSrc.right = 600;
	m_pSite->Invalidate( );
  	return S_OK;
}

STDMETHODIMP cDerethMap::Reformat()
{
   return S_OK;
}

STDMETHODIMP cDerethMap::Render( ICanvas *pCanvas )
{
   _ASSERTE( pCanvas != NULL );
   _ASSERTE( m_pSite.p != NULL );

	static POINT ptOff = { 0, 0 };

	RECT rcPos;
	m_pSite->get_Position( &rcPos );

	RECT rcClient = { 0, 12, rcPos.right - rcPos.left, rcPos.bottom - rcPos.top + 12 };

	RECT rcDest = { 0, 12, rcPos.right - rcPos.left, rcPos.bottom - rcPos.top + 12 };

	if( m_pBackground.p != NULL )
		m_pBackground->StretchBltArea( &m_rcMapSrc, pCanvas, &rcDest );

	checkFont( );
	
	return S_OK;
}
