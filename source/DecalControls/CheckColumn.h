// CheckColumn.h : Declaration of the cCheckColumn

#ifndef __CHECKCOLUMN_H_
#define __CHECKCOLUMN_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cCheckColumn
class ATL_NO_VTABLE cCheckColumn : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cCheckColumn, &CLSID_CheckColumn>,
	public IListColumn
{
public:
	cCheckColumn()
	{
	}

   CComPtr< IList > m_pList;
   CComPtr< IPluginSite > m_pSite;

DECLARE_REGISTRY_RESOURCEID(IDR_CHECKCOLUMN)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cCheckColumn)
	COM_INTERFACE_ENTRY(IListColumn)
END_COM_MAP()

// IListColumn
public:
	STDMETHOD(get_Color)(long nRow, /*[out, retval]*/ long *pVal);
	STDMETHOD(put_Color)(long nRow, /*[in]*/ long newVal);
	STDMETHOD(Activate)(LPPOINT ptCell);
	STDMETHOD(get_Height)(/*[out, retval]*/ long *pVal);
	STDMETHOD(SchemaLoad)(IUnknown *pSchema);
	STDMETHOD(Initialize)(/*[in]*/ IList * newVal, IPluginSite *pSite);
	STDMETHOD(get_DataColumns)(/*[out, retval]*/ long *pVal);
	STDMETHOD(Render)(ICanvas *, LPPOINT ptCell, long nColor);
	STDMETHOD(get_Width)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_FixedWidth)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Width)(/*[in]*/ long newVal);
};

#endif //__CHECKCOLUMN_H_
