// Client.cpp : Implementation of cClient
#include "stdafx.h"
#include "DecalControls.h"
#include "FixedLayout.h"

/////////////////////////////////////////////////////////////////////////////
// cClient

void cFixedLayout::onSchemaLoad( IView *pView, MSXML::IXMLDOMElementPtr &pElement )
{
   // Create all child controls from schema and let them be
   // placed wherever they like
   MSXML::IXMLDOMElementPtr pChild;
   for( MSXML::IXMLDOMNodeListPtr pChildren = pElement->selectNodes( _T( "control" ) );
         ( pChild = pChildren->nextNode() ).GetInterfacePtr() != NULL; )
      loadChildControl( pView, pChild );
}

STDMETHODIMP cFixedLayout::CreateChild(LayerParams *params, ILayer *pSink)
{
   m_pSite->CreateChild( params, pSink );

	return S_OK;
}

STDMETHODIMP cFixedLayout::PositionChild(long nID, LPRECT prcNew)
{
   CComPtr< ILayerSite > pChildSite;
   HRESULT hRes = m_pSite->get_Child( nID, ePositionByID, &pChildSite );

   _ASSERTE( SUCCEEDED( hRes ) );

   pChildSite->put_Position( prcNew );

	return S_OK;
}
