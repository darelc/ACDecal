// ListView.h : Declaration of the cListView

#ifndef __LISTVIEW_H_
#define __LISTVIEW_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"

class cList;

/////////////////////////////////////////////////////////////////////////////
// cListView
class ATL_NO_VTABLE cListView : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public ILayerImpl< cListView >,
   public ILayerRenderImpl,
   public ILayerMouseImpl,
   public cNoEventsImpl,
	public IListView
{
public:
	cListView();

   CComPtr< ICanvas > m_pCache;

   CComPtr< IImageCache > m_pBackground;

	typedef std::map< std::pair< long, long >, long > cCellMap;
	cCellMap m_cells;

   long m_nRowHeight,
      m_nRowCache,
      m_nValidFrom,
      m_nValidTo;

   cList *m_pList;

   void onCreate();
   void onDestroy();

BEGIN_COM_MAP(cListView)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IListView)
END_COM_MAP()

public:
	STDMETHOD(get_Color)(long nCol, long nRow, /*[out, retval]*/ long *pVal);
	STDMETHOD(put_Color)(long nCol, long nRow, /*[in]*/ long newVal);
   // IListView Methods
	STDMETHOD(put_Area)(/*[in]*/ LPSIZE newVal);
	STDMETHOD(SetCacheInfo)(long nRowHeight, long nRowsToCache);
	STDMETHOD(InvalidateFrom)(long nRow);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
   STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
   STDMETHOD(MouseDown)(MouseState *pMS);
};

#endif //__LISTVIEW_H_
