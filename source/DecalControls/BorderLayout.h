// BorderLayout.h : Declaration of the cBorderLayout

#ifndef __BORDERLAYOUT_H_
#define __BORDERLAYOUT_H_

#include "resource.h"       // main symbols
#include "DecalControlsCP.h"

#include "ControlImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cBorderLayout
class ATL_NO_VTABLE cBorderLayout : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cBorderLayout, &CLSID_BorderLayout>,
   public ILayerImpl< cBorderLayout >,
   public ILayoutImpl< cBorderLayout, IBorderLayout, &IID_ILayout, &LIBID_DecalControls >,
   public IProvideClassInfo2Impl< &CLSID_BorderLayout, &DIID_IControlEvents, &LIBID_DecalControls >,
	public IConnectionPointContainerImpl<cBorderLayout>,
	public CProxyIControlEvents< cBorderLayout >
{
public:
	cBorderLayout()
      : m_nCenterID( -1 )
	{
	}

   // Data members
   struct cEdgePlacement
   {
      eBorderEdge m_edge;
      long m_nSize;
      long m_nID;
   };

   typedef std::list< cEdgePlacement > cEdgePlacementList;
   cEdgePlacementList m_edges;
   long m_nCenterID;

   void onChildDestroy( long nID );
   void onSchemaLoad( IView *pView, MSXML::IXMLDOMElementPtr &pElement );

DECLARE_REGISTRY_RESOURCEID(IDR_BORDERLAYOUT)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cBorderLayout)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(ILayerSchema)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(ILayout)
	COM_INTERFACE_ENTRY(IBorderLayout)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cBorderLayout)
CONNECTION_POINT_ENTRY(DIID_IControlEvents)
END_CONNECTION_POINT_MAP()

public:
   // IBorderLayout Methods
	STDMETHOD(CreateCenter)(ILayer *pSink);
	STDMETHOD(CreateEdge)(eBorderEdge eEdge, long nSize, ILayer *pSink);

   // ILayerRender Methods
   STDMETHOD(Reformat)();
};

#endif //__BORDERLAYOUT_H_
