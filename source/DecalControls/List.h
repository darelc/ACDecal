// List.h : Declaration of the cList

#ifndef __LIST_H_
#define __LIST_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "DecalControlsCP.h"

/////////////////////////////////////////////////////////////////////////////
// cList
class ATL_NO_VTABLE cList : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cList, &CLSID_List>,
	public IControlImpl< cList, IList, &IID_IListDisp, &LIBID_DecalControls >,
   public ILayerImpl< cList >,
   public ILayerRenderImpl,
   public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_List, &DIID_IListEvents, &LIBID_DecalControls >,
   public IConnectionPointContainerImpl<cList>,
   public CProxyIListEvents< cList >
{
public:
	cList()
	{
		m_bAutoScroll = false;
	}

   class cColumn
   {
   public:
      CComPtr< IListColumn > m_pColumn;
      long m_nLeft, m_nRight;
      long m_nDataLeft, m_nDataRight;

      bool m_bSupportsMouse;
   };

   typedef std::deque< cColumn > cColumnList;

   class cRow
   {
   public:
      mutable VARIANT *m_pData,
         *m_pEndData;
      bool m_bInvalid;
      mutable long *m_colors; /* cyn - 07/08/2002 */
      
      explicit cRow( long nDataWidth );
      cRow( const cRow &row );
      ~cRow();

      cRow &operator=( const cRow & );
   };

	typedef std::vector< cRow > cRowList;

   CComPtr< IScroller > m_pScroller;
   CComPtr< IListView > m_pView;

   cColumnList m_cols;
   cRowList m_rows;

   long m_nRowHeight;
   bool m_bAutoScroll;

   void onCreate();
   void onDestroy();

DECLARE_REGISTRY_RESOURCEID(IDR_LIST)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cList)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(ILayerSchema)
	COM_INTERFACE_ENTRY(IList)
   COM_INTERFACE_ENTRY(IListDisp)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cList)
CONNECTION_POINT_ENTRY(DIID_IListEvents)
END_CONNECTION_POINT_MAP()

public:
	STDMETHOD(JumpToPosition)(long newVal);
	STDMETHOD(get_ScrollPosition)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_ScrollPosition)(/*[in]*/ long newVal);
	STDMETHOD(get_AutoScroll)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_AutoScroll)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_Color)(long nX, long nY, /*[out, retval]*/ long *pVal);
	STDMETHOD(put_Color)(long nX, long nY, /*[in]*/ long newVal);
	STDMETHOD(InsertRow)(long lIndex);
   // IList Methods
	STDMETHOD(get_CellRect)(LPPOINT pt, /*[out, retval]*/ LPRECT pVal);
	STDMETHOD(get_Data)(long nX, long nY, /*[optional, defaultvalue(0)]*/ long nSubValue, /*[out, retval]*/ VARIANT* pVal);
	STDMETHOD(put_Data)(long nX, long nY, /*[optional, defaultvalue(0)]*/ long nSubValue, /*[in]*/ VARIANT* newVal);
	STDMETHOD(DeleteRow)(long nIndex);
	STDMETHOD(AddRow)(/*[out, retval]*/ long *pnNewIndex);
	STDMETHOD(AddColumn)(IListColumn *pNewColumn, /*[out, retval]*/ long *nIndex);
	STDMETHOD(put_RowEstimate)(/*[in]*/ long newVal);
    STDMETHOD(get_Count)(long *pnCount);
	STDMETHOD(get_CountCols)(long *pnCount);
	STDMETHOD(Clear)();
	STDMETHOD(put_ColumnWidth)(long nColumn, long nWidth);
	STDMETHOD(get_ColumnWidth)(long nColumn, long *nWidth);
 
   // ILayerRender Methods
   STDMETHOD(Reformat)();

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *, IUnknown *pSchema);
};

#endif //__LIST_H_
