# Microsoft Developer Studio Project File - Name="DecalControls" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=DecalControls - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DecalControls.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DecalControls.mak" CFG="DecalControls - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DecalControls - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "DecalControls - Win32 Release MinDependency" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DecalControls - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GX /ZI /Od /I "..\Inject" /I "..\Include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD MTL /nologo /I "..\Include" /Oicf
# ADD BASE RSC /l 0x1009 /d "_DEBUG"
# ADD RSC /l 0x1009 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386
# Begin Custom Build - Performing registration
OutDir=.\..\Debug
TargetPath=\Decal\source\Debug\DecalControls.dll
InputPath=\Decal\source\Debug\DecalControls.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "DecalControls - Win32 Release MinDependency"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ReleaseMinDependency"
# PROP BASE Intermediate_Dir "ReleaseMinDependency"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Release"
# PROP Intermediate_Dir "ReleaseMinDependency"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /D "_ATL_MIN_CRT" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /Zi /Oa /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "..\Inject" /I "..\Include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /c
# ADD MTL /nologo /I "..\Include" /Oicf
# ADD BASE RSC /l 0x1009 /d "NDEBUG"
# ADD RSC /l 0x1009 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept /libpath:"..\Release"
# Begin Custom Build - Performing registration
OutDir=.\..\Release
TargetPath=\Decal\source\Release\DecalControls.dll
InputPath=\Decal\source\Release\DecalControls.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ENDIF 

# Begin Target

# Name "DecalControls - Win32 Debug"
# Name "DecalControls - Win32 Release MinDependency"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\BorderLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\Checkbox.cpp
# End Source File
# Begin Source File

SOURCE=.\CheckColumn.cpp
# End Source File
# Begin Source File

SOURCE=.\Choice.cpp
# End Source File
# Begin Source File

SOURCE=.\ChoiceDropDown.cpp
# End Source File
# Begin Source File

SOURCE=.\ChoicePopup.cpp
# End Source File
# Begin Source File

SOURCE=.\DecalControls.cpp
# End Source File
# Begin Source File

SOURCE=.\DecalControls.def
# End Source File
# Begin Source File

SOURCE=.\DecalControls.idl
# ADD MTL /tlb ".\DecalControls.tlb" /h "DecalControls.h" /iid "DecalControls_i.c" /Oicf
# End Source File
# Begin Source File

SOURCE=.\DecalControls.rc
# End Source File
# Begin Source File

SOURCE=.\DerethMap.cpp
# End Source File
# Begin Source File

SOURCE=.\Edit.cpp
# End Source File
# Begin Source File

SOURCE=.\FixedLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\IconColumn.cpp
# End Source File
# Begin Source File

SOURCE=..\Inject\Inject_i.c
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\List.cpp
# End Source File
# Begin Source File

SOURCE=.\ListView.cpp
# End Source File
# Begin Source File

SOURCE=.\Notebook.cpp
# End Source File
# Begin Source File

SOURCE=.\PageLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\Progress.cpp
# End Source File
# Begin Source File

SOURCE=.\PushButton.cpp
# End Source File
# Begin Source File

SOURCE=.\Scroller.cpp
# End Source File
# Begin Source File

SOURCE=.\Slider.cpp
# End Source File
# Begin Source File

SOURCE=.\Static.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TextColumn.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\BorderLayout.h
# End Source File
# Begin Source File

SOURCE=.\Checkbox.h
# End Source File
# Begin Source File

SOURCE=.\CheckColumn.h
# End Source File
# Begin Source File

SOURCE=.\Choice.h
# End Source File
# Begin Source File

SOURCE=.\ChoiceDropDown.h
# End Source File
# Begin Source File

SOURCE=.\ChoicePopup.h
# End Source File
# Begin Source File

SOURCE=.\ControlImpl.h
# End Source File
# Begin Source File

SOURCE=.\DecalControlsCP.h
# End Source File
# Begin Source File

SOURCE=.\DerethMap.h
# End Source File
# Begin Source File

SOURCE=.\Edit.h
# End Source File
# Begin Source File

SOURCE=.\FixedLayout.h
# End Source File
# Begin Source File

SOURCE=.\IconColumn.h
# End Source File
# Begin Source File

SOURCE=.\List.h
# End Source File
# Begin Source File

SOURCE=.\ListView.h
# End Source File
# Begin Source File

SOURCE=.\Notebook.h
# End Source File
# Begin Source File

SOURCE=.\PageLayout.h
# End Source File
# Begin Source File

SOURCE=.\Progress.h
# End Source File
# Begin Source File

SOURCE=.\PushButton.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\Scroller.h
# End Source File
# Begin Source File

SOURCE=.\Slider.h
# End Source File
# Begin Source File

SOURCE=.\Static.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TextColumn.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\BorderLayout.rgs
# End Source File
# Begin Source File

SOURCE=.\Checkbox.rgs
# End Source File
# Begin Source File

SOURCE=.\CheckColumn.rgs
# End Source File
# Begin Source File

SOURCE=.\Choice.rgs
# End Source File
# Begin Source File

SOURCE=.\ChoiceDropDown.rgs
# End Source File
# Begin Source File

SOURCE=.\Client.rgs
# End Source File
# Begin Source File

SOURCE=.\DerethMap.rgs
# End Source File
# Begin Source File

SOURCE=.\Edit.rgs
# End Source File
# Begin Source File

SOURCE=.\FixedLayout.rgs
# End Source File
# Begin Source File

SOURCE=.\IconColumn.rgs
# End Source File
# Begin Source File

SOURCE=.\List.rgs
# End Source File
# Begin Source File

SOURCE=.\Notebook.rgs
# End Source File
# Begin Source File

SOURCE=.\PageLayout.rgs
# End Source File
# Begin Source File

SOURCE=.\Progress.rgs
# End Source File
# Begin Source File

SOURCE=.\PushButton.rgs
# End Source File
# Begin Source File

SOURCE=.\Scroller.rgs
# End Source File
# Begin Source File

SOURCE=.\Slider.rgs
# End Source File
# Begin Source File

SOURCE=.\Static.rgs
# End Source File
# Begin Source File

SOURCE=.\TextColumn.rgs
# End Source File
# End Group
# End Target
# End Project
