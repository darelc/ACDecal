// IconColumn.cpp : Implementation of cIconColumn
#include "stdafx.h"
#include "DecalControls.h"
#include "IconColumn.h"

/////////////////////////////////////////////////////////////////////////////
// cIconColumn


STDMETHODIMP cIconColumn::get_FixedWidth(VARIANT_BOOL *pVal)
{
   *pVal = VARIANT_TRUE;

	return S_OK;
}

STDMETHODIMP cIconColumn::get_Width(long *pVal)
{
   *pVal = 20;

	return S_OK;
}


STDMETHODIMP cIconColumn::put_Width(long newVal)
{
    //m_Width = newVal;

	return S_OK;
}

STDMETHODIMP cIconColumn::Render(ICanvas *pCanvas, LPPOINT ptCell, long nColor)
{
   _variant_t vIconLib, vIcon;

   m_pList->get_Data( ptCell->x, ptCell->y, 0, &vIconLib );
   m_pList->get_Data( ptCell->x, ptCell->y, 1, &vIcon );

   // Converted nicely now ...
   // Find our checked and unchecked images
   CComPtr< IIconCache > pIcons;

   SIZE sz = { 16, 16 };
   m_pSite->GetIconCache( &sz, &pIcons );

   POINT pt = { 2, 2 };
   pIcons->DrawIcon( &pt, static_cast< long >( vIcon ), static_cast< long >( vIconLib ), pCanvas );

	return S_OK;
}

STDMETHODIMP cIconColumn::get_DataColumns(long *pVal)
{
   *pVal = 2;

	return S_OK;
}

STDMETHODIMP cIconColumn::Initialize(IList *newVal, IPluginSite *pSite)
{
   m_pList = newVal;
   m_pSite = pSite;

	return S_OK;
}

STDMETHODIMP cIconColumn::SchemaLoad(IUnknown *pSchema)
{
	// TODO: Add your implementation code here

	return S_OK;
}

STDMETHODIMP cIconColumn::get_Height(long *pVal)
{
   *pVal = 20;

	return S_OK;
}

STDMETHODIMP cIconColumn::Activate(LPPOINT ptCell)
{
	// TODO: Add your implementation code here
	return S_OK;
}

STDMETHODIMP cIconColumn::get_Color(long nRow, long *pVal)
{
	return S_OK;
}

STDMETHODIMP cIconColumn::put_Color(long nRow, long newVal)
{
	return S_OK;
}
