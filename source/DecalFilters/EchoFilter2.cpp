// EchoFilter.cpp : Implementation of cEchoFilter
#include "stdafx.h"
#include "DecalFilters.h"
#include "EchoFilter2.h"

/////////////////////////////////////////////////////////////////////////////
// cEchoFilter


STDMETHODIMP cEchoFilter2::DispatchServer(IMessage2 *pMsg)
{
   Fire_EchoServer( pMsg );

	return S_OK;
}

STDMETHODIMP cEchoFilter2::DispatchClient(IMessage2 *pMsg)
{
   Fire_EchoClient( pMsg );

	return S_OK;
}
