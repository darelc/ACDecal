// DecalFilters.idl : IDL source for DecalFilters.dll
//

// This file will be processed by the MIDL tool to
// produce the type library (DecalFilters.tlb) and marshalling code.

import "oaidl.idl";
import "ocidl.idl";
import "DecalNet.idl";

enum eTrainingType
{
	eTrainUnusable,
	eTrainUntrained,
	eTrainTrained,
	eTrainSpecialized 
};

enum eAttributeID
{
   eAttrStrength = 1,
   eAttrEndurance = 2,
   eAttrQuickness = 3,
   eAttrCoordination = 4,
   eAttrFocus = 5,
   eAttrSelf = 6,
   eAttrNULL = 255
};

enum eSkillID
{
	eSkillAxe = 1,
	eSkillBow = 2,
	eSkillCrossbow = 3,
	eSkillDagger = 4,
	eSkillMace = 5,
	eSkillMeleeDefense = 6,
	eSkillMissileDefense = 7,
	eSkillSpear = 9,
	eSkillStaff = 10,
	eSkillSword = 11,
	eSkillThrownWeapons = 12,
	eSkillUnarmed = 13,
	eSkillArcaneLore = 14,
	eSkillMagicDefense = 15,
	eSkillManaConversion = 16,
	eSkillItemTinkering = 18,
	eSkillAssessPerson = 19,
	eSkillDeception = 20,
	eSkillHealing = 21,
	eSkillJump = 22,
	eSkillLockpick = 23,
	eSkillRun = 24,
	eSkillAssessCreature = 27,
	eSkillWeaponTinkering = 28,
	eSkillArmorTinkering = 29,
	eSkillMagicItemTinkering = 30,
	eSkillCreatureEnchantment = 31,
	eSkillItemEnchantment = 32,
	eSkillLifeMagic = 33,
	eSkillWarMagic = 34,
	eSkillLeadership = 35,
	eSkillLoyalty = 36,
	eSkillFletching = 37,
	eSkillAlchemy = 38,
	eSkillCooking = 39
};

enum eVitalID
{
	eHealth = 1,
	eStamina = 2,
	eMana = 3
};

enum eStatisticType
{
	eStatBurden = 5,
	eStatPyreal = 20,
	eStatExperience = 21,
	eStatUnassignedExp = 22,
	eStatUnassignedSkillPoints = 24,
	eStatLevel = 25,
	eStatRank = 30
};

enum eStringType
{
	eStringName = 1,
	eStringGender = 3,
	eStringRace = 4,
	eStringClass = 5
};

enum eObjectType
{
	eMeleeWeapon = 0,
	eArmor,
	eClothing,
	eJewelry,
	eMonster,
	eFood,
	eMoney,
	eMisc,
	eMissileWeapon,
	eContainer,
	eGem,
	eSpellComponent,
	eKey,
	ePortal,
	eTradeNote,
	eManaStone,
	eService,
	ePlant,
	eBaseCooking,
	eBaseAlchemy,
	eBaseFletching,
	eCraftedCooking,
	eCraftedAlchemy,
	eCraftedFletching,
	ePlayer,
	eVendor,
	eDoor,
	eCorpse,
	eLifestone,
	eHealingKit,
	eLockpick,
	eUnknownObject,
	eWandStaffOrb,
	eBundle,
	eBook,
	eJournal,
	eSign,
	eHousing,
	eNPC,
	eFoci,
	eSalvage,
	eUst,

	// Leave this last!!!
	eNumObjectTypes
};

	[
		uuid(A67C748D-2427-4a13-A114-7ACC1D4C9433),
		helpstring("IPrefilterEvents Interface")
	]
	dispinterface IPrefilterEvents
	{
	properties:
	methods:
		[id(1), helpstring("method Event")] void Event(long ID, IMessage *Message);
	};

	[
		object,
		uuid(B6496852-E977-4DA2-884D-09AFEA3D7582),
		dual,
		helpstring("IAttributeInfo Interface"),
		pointer_default(unique)
	]
	interface IAttributeInfo : IDispatch
	{
		[propget, id(1), helpstring("property Name")] HRESULT Name([out, retval] BSTR *pVal);
		[propget, id(2), helpstring("property Creation")] HRESULT Creation([out, retval] long *pVal);
		[propget, id(3), helpstring("property Exp")] HRESULT Exp([out, retval] long *pVal);
		[propget, id(4), helpstring("property Current")] HRESULT Current([out, retval] long *pVal);
	};

	[
		object,
		uuid(6FBA5326-F234-4AEC-B844-2136A0D50FD5),
		dual,
		helpstring("ISkillInfo Interface"),
		pointer_default(unique)
	]
	interface ISkillInfo : IDispatch
	{
		[propget, id(1), helpstring("property Name")] HRESULT Name([out, retval] BSTR *pVal);
		[propget, id(2), helpstring("property ShortName")] HRESULT ShortName([out, retval] BSTR *pVal);
		[propget, id(3), helpstring("property Formula")] HRESULT Formula([out, retval] BSTR *pVal);
		[propget, id(4), helpstring("property Base")] HRESULT Base([out, retval] long *pVal);
		[propget, id(5), helpstring("property Current")] HRESULT Current([out, retval] long *pVal);
		[propget, id(6), helpstring("property Exp")] HRESULT Exp([out, retval] long *pVal);
		[propget, id(7), helpstring("property Training")] HRESULT Training([out, retval] enum eTrainingType *pVal);
		[propget, id(8), helpstring("property Known")] HRESULT Known([out, retval] VARIANT_BOOL *pVal);
		[propget, id(9), helpstring("property Increment")] HRESULT Increment([out, retval] long *pVal);
		[propget, id(10), helpstring("property Bonus")] HRESULT Bonus([out, retval] long *pVal);
	};

	[
		object,
		uuid(B83155F4-E298-4004-80C6-AA592F677430),
		dual,
		helpstring("IEnchantment Interface"),
		pointer_default(unique)
	]
	interface IEnchantment : IDispatch
	{
		[propget, id(1), helpstring("property SpellID")] HRESULT SpellID([out, retval] long *pVal);
		[propput, id(1), helpstring("property SpellID")] HRESULT SpellID([in] long newVal);
		[propget, id(2), helpstring("property Layer")] HRESULT Layer([out, retval] long *pVal);
		[propput, id(2), helpstring("property Layer")] HRESULT Layer([in] long newVal);
		[propget, id(3), helpstring("property TimeRemaining")] HRESULT TimeRemaining([out, retval] long *pVal);
		[propput, id(3), helpstring("property TimeRemaining")] HRESULT TimeRemaining([in] long newVal);
		[propget, id(4), helpstring("property Affected")] HRESULT Affected([out, retval] long *pVal);
		[propput, id(4), helpstring("property Affected")] HRESULT Affected([in] long newVal);
		[propget, id(5), helpstring("property AffectedMask")] HRESULT AffectedMask([out, retval] long *pVal);
		[propput, id(5), helpstring("property AffectedMask")] HRESULT AffectedMask([in] long newVal);
		[propget, id(6), helpstring("property Family")] HRESULT Family([out, retval] long *pVal);
		[propput, id(6), helpstring("property Family")] HRESULT Family([in] long newVal);
		[propget, id(7), helpstring("property Adjustment")] HRESULT Adjustment([out, retval] double *pVal);
		[propput, id(7), helpstring("property Adjustment")] HRESULT Adjustment([in] double newVal);
	};
	
	[
		object,
		uuid(C8A16199-B649-41F3-BFCA-A1BD6E2E1F2C),
		dual,
		helpstring("IAllegianceInfo Interface"),
		pointer_default(unique)
	]
	interface IAllegianceInfo : IDispatch
	{
		[propget, id(1), helpstring("property Name")] HRESULT Name([out, retval] BSTR *pVal);
		[propget, id(2), helpstring("property GUID")] HRESULT GUID([out, retval] long *pVal);
		[propget, id(3), helpstring("property TreeParent")] HRESULT TreeParent([out, retval] long *pVal);
		[propget, id(4), helpstring("property Type")] HRESULT Type([out, retval] long *pVal);
		[propget, id(5), helpstring("property XP")] HRESULT XP([out, retval] long *pVal);
		[propget, id(6), helpstring("property Loyalty")] HRESULT Loyalty([out, retval] long *pVal);
		[propget, id(7), helpstring("property Leadership")] HRESULT Leadership([out, retval] long *pVal);
		[propget, id(8), helpstring("property Gender")] HRESULT Gender([out, retval] long *pVal);
		[propget, id(9), helpstring("property Race")] HRESULT Race([out, retval] long *pVal);
		[propget, id(10), helpstring("property Rank")] HRESULT Rank([out, retval] long *pVal);
		[propget, id(11), helpstring("property Unknown")] HRESULT Unknown([out, retval] double *pVal);
	};

	[
		object,
		uuid(0CE57594-4E30-4446-956D-CE460C7355AF),
		dual,
		helpstring("ICharacterStats Interface"),
		pointer_default(unique)
	]
	interface ICharacterStats : IDispatch
	{
		[propget, id(1), helpstring("property Character")] HRESULT Character([out, retval] long *pVal);
		[propget, id(2), helpstring("property Level")] HRESULT Level([out, retval] long *pVal);
		[propget, id(3), helpstring("property Rank")] HRESULT Rank([out, retval] long *pVal);
		[propget, id(4), helpstring("property TotalExp")] HRESULT TotalExp([out, retval] long *pVal);
		[propget, id(5), helpstring("property UnassignedExp")] HRESULT UnassignedExp([out, retval] long *pVal);
		[propget, id(6), helpstring("property SkillPoints")] HRESULT SkillPoints([out, retval] long *pVal);
		[propget, id(7), helpstring("property Server")] HRESULT Server([out, retval] BSTR *pVal);
		[propget, id(8), helpstring("property Name")] HRESULT Name([out, retval] BSTR *pVal);
		[propget, id(9), helpstring("property Race")] HRESULT Race([out, retval] BSTR *pVal);
		[propget, id(10), helpstring("property Gender")] HRESULT Gender([out, retval] BSTR *pVal);
		[propget, id(11), helpstring("property ClassTemplate")] HRESULT ClassTemplate([out, retval] BSTR *pVal);
		[propget, id(12), helpstring("property AttributeCount")] HRESULT AttributeCount([out, retval] long *pVal);
		[propget, id(13), helpstring("property SkillCount")] HRESULT SkillCount([out, retval] long *pVal);
		[propget, id(14), helpstring("property VitalCount")] HRESULT VitalCount([out, retval] long *pVal);
		[propget, id(15), helpstring("property Attribute")] HRESULT Attribute(enum eAttributeID Index, [out, retval] IAttributeInfo * *pVal);
		[propget, id(16), helpstring("property Skill")] HRESULT Skill(enum eSkillID Index, [out, retval] ISkillInfo * *pVal);
		[propget, id(17), helpstring("property Vital")] HRESULT Vital(enum eVitalID Index, [out, retval] ISkillInfo * *pVal);
		[propget, id(18), helpstring("property SpellLearned")] HRESULT SpellLearned(long SpellID, [out, retval] long *pVal);
		[propget, id(19), helpstring("property TotalSpells")] HRESULT TotalSpells([out, retval] long *pVal);
		[propget, id(20), helpstring("property Health")] HRESULT Health([out, retval] long *pVal);
		[propget, id(21), helpstring("property Stamina")] HRESULT Stamina([out, retval] long *pVal);
		[propget, id(22), helpstring("property Mana")] HRESULT Mana([out, retval] long *pVal);
		[propget, id(23), helpstring("property Birth")] HRESULT Birth([out, retval] long *pVal);
		[propget, id(24), helpstring("property Age")] HRESULT Age([out, retval] long *pVal);
		[propget, id(25), helpstring("property Deaths")] HRESULT Deaths([out, retval] long *pVal);
		[propget, id(26), helpstring("property VassalCount")] HRESULT VassalCount([out, retval] long *pVal);
		[propget, id(27), helpstring("property Monarch")] HRESULT Monarch([out, retval] IAllegianceInfo **pVal);
		[propget, id(28), helpstring("property Patron")] HRESULT Patron([out, retval] IAllegianceInfo **pVal);
		[propget, id(29), helpstring("property MyAllegiance")] HRESULT MyAllegiance([out, retval] IAllegianceInfo **pVal);
		[propget, id(30), helpstring("property Vassal")] HRESULT Vassal(long VassalNum, [out, retval] IAllegianceInfo **pVal);
		[propget, id(31), helpstring("property Followers")] HRESULT Followers([out, retval] long *pVal);
		[propget, id(32), helpstring("property MonarchFollowers")] HRESULT MonarchFollowers([out, retval] long *pVal);
		[propget, id(33), helpstring("property EnchantmentCount")] HRESULT EnchantmentCount([out, retval] long *pVal);
		[propget, id(34), helpstring("property Enchantment")] HRESULT Enchantment(long EnchantNum, [out, retval] IEnchantment **pVal);
		[propget, id(35), helpstring("property EffectiveAttribute")] HRESULT EffectiveAttribute(enum eAttributeID Index, [out, retval] long *pVal);
		[propget, id(36), helpstring("property EffectiveSkill")] HRESULT EffectiveSkill(enum eSkillID Index, [out, retval] long *pVal);
		[propget, id(37), helpstring("property EffectiveVital")] HRESULT EffectiveVital(enum eVitalID Index, [out, retval] long *pVal);
		[propget, id(38), helpstring("property Vitae")] HRESULT Vitae([out, retval] long *pVal);
		[propget, id(39), helpstring("property BurdenUnits")] HRESULT BurdenUnits([out, retval] long *pVal);
		[propget, id(40), helpstring("property Burden")] HRESULT Burden([out, retval] long *pVal);
	};

	[
		uuid(7B7FEF07-DAE2-46f3-AC71-7E47F8EF3A44),
		helpstring("IEchoSink2 Interface"),
		dual,
		pointer_default(unique)
	]
	interface IEcho : IDispatch
	{
	};

	[
		uuid(236B3F19-8F40-492b-A462-0EB4447A6296),
		helpstring("IEchoSink Interface")
	]
	dispinterface IEchoSink
	{
	properties:
	methods:
		[id(1), helpstring("method EchoMessage")] void EchoMessage(IMessage *pMsg);
	};

	[
		uuid(002C8BAE-917E-4978-B223-B45B3E8B15E6),
		helpstring("IEchoSink2 Interface"),
		dual,
		pointer_default(unique)
	]
	interface IEcho2 : IDispatch
	{
	};

	[
		uuid(6C9BCC85-BAD0-4706-9CC5-D491E43F81C4),
		helpstring("IEchoSink2 Interface")
	]
	dispinterface IEchoSink2
	{
	properties:
	methods:
		[id(1), helpstring("method EchoServer")] void EchoServer(IMessage2 *pMsg);
		[id(2), helpstring("method EchoClient")] void EchoClient(IMessage2 *pMsg);
	};

[
	object,
	uuid(6FD2E07C-7E9C-451B-8968-2FCB0226D0E8),
	dual,
	nonextensible,
	helpstring("IIdentifyQueue Interface"),
	pointer_default(unique)
]
interface IIdentifyQueue : IDispatch{
};
[
	uuid(DA16DAA9-7F16-45D9-A59F-8C45A7F2ACB1),
	version(1.0),
	helpstring("Decal Network Filters Type Library")
]
library DecalFilters
{
	importlib("stdole32.tlb");
	importlib("stdole2.tlb");

	dispinterface IEchoSink;
	dispinterface IEchoSink2;
	dispinterface IPrefilterEvents;
	dispinterface ICharacterStatsEvents;

	interface IWorldIterator;
	interface IWorldObject;
	interface IWorldObject2;
	interface IEnchantment;
	interface IAllegianceInfo;

	[
		uuid(0B60F187-13CD-4E35-B8A2-FE128F05CA6B),
		helpstring("_ICharacterStatsEvents Interface")
	]
	dispinterface ICharacterStatsEvents
	{
		properties:
		methods:
		[id(1), helpstring("method Login")] HRESULT Login(long character);
		[id(2), helpstring("method Spellbook_Add")] HRESULT Spellbook_Add(long SpellAdded);
		[id(3), helpstring("method Spellbook_Delete")] HRESULT Spellbook_Delete(long SpellDeleted);
	};

	[
		object,
		uuid(3C1CBEF8-E72A-4BDF-B92E-4F3307109766),
		dual,
		helpstring("IPrefilter Interface"),
		pointer_default(unique)
	]
	interface IPrefilter : IDispatch
	{
	};
	[
		object,
		uuid(C5F0FF7E-A043-4A28-8893-F6E43F4E9DED),
		dual,
		helpstring("IWorld Interface"),
		pointer_default(unique)
	]
	interface IWorld : IDispatch
	{
		[propget, id(1), helpstring("property Object")] HRESULT Object(long GUID, [out, retval] IWorldObject **pVal);
		[propget, id(2), helpstring("property ByName")] HRESULT ByName(BSTR Name, [out, retval] IWorldIterator **pVal);
		[propget, id(3), helpstring("property All")] HRESULT All([out, retval] IWorldIterator **pVal);
		[propget, id(4), helpstring("property ByType")] HRESULT ByType(enum eObjectType Type, [out, retval] IWorldIterator * *pVal);
		[propget, id(5), helpstring("property ByNameSubstring")] HRESULT ByNameSubstring(BSTR Substring, [out, retval] IWorldIterator * *pVal);
		[propget, id(6), helpstring("property ByContainer")] HRESULT ByContainer(long GUID, [out, retval] IWorldIterator * *pVal);
		[propget, id(7), helpstring("property Inventory")] HRESULT Inventory([out, retval] IWorldIterator * *pVal);
		[id(8),	helpstring("method Distance2D")] HRESULT Distance2D(long GUID1, long GUID2, [out, retval] float* pVal);
		[propget, id(9), helpstring("property ByOwner")] HRESULT ByOwner(long GUID, [out, retval] IWorldIterator * *pVal);
		[propget, id(10), helpstring("property Landscape")] HRESULT Landscape([out, retval] IWorldIterator * *pVal);
		[propget, id(11), helpstring("property NumObjectTypes")] HRESULT NumObjectTypes([out, retval] long *lVal);
	};
	[
		uuid(4540C969-08D1-46BF-97AD-6B19D3C10BEE),
		helpstring("CharacterStats Class")
	]
	coclass CharacterStats
	{
		[default] interface ICharacterStats;
		[default, source] dispinterface ICharacterStatsEvents;
	};

	[
		uuid(AF42E9D7-E3F3-416B-AF32-A411F3F6EE72),
		helpstring("AttributeInfo Class"),
		noncreatable
	]
	coclass AttributeInfo
	{
		[default] interface IAttributeInfo;
	};

	[
		uuid(652DA384-AA3B-4F9D-9730-8CF753DA1A31),
		helpstring("SkillInfo Class"),
		noncreatable
	]
	coclass SkillInfo
	{
		[default] interface ISkillInfo;
	};

	[
		uuid(5DCF056E-B7F5-4AEB-93A0-628617B28488),
		helpstring("Enchantment Class"),
		noncreatable
	]
	coclass Enchantment
	{
		[default] interface IEnchantment;
	};
	
	[
		uuid(B2EB5B5A-5E3F-4F60-8BC0-BB729FFC04F6),
		helpstring("AllegianceInfo Class"),
		noncreatable
	]
	coclass AllegianceInfo
	{
		[default] interface IAllegianceInfo;
	};

	[
		uuid(443D4A68-5422-4E0C-9460-973F8FBDB190),
		helpstring("Prefilter Class")
	]
	coclass Prefilter
	{
		[default] interface IPrefilter;
		[default, source] dispinterface IPrefilterEvents;
	};

	[
		uuid(8C2FA400-315D-41DE-B063-D6EF04F12E1F),
		helpstring("EchoFilter Class")
	]
	coclass EchoFilter
	{
		[default] interface IEcho;
		[default, source] dispinterface IEchoSink;
	};

	[
		uuid(34239EAD-6317-4c40-A405-193BA5232DD8),
		helpstring("EchoFilter2 Class")
	]
	coclass EchoFilter2
	{
		[default] interface IEcho2;
		[default, source] dispinterface IEchoSink2;
	};

	[
		object,
		uuid(02EA40F2-59C0-4396-8B4C-B8D45EC0CC98),
		dual,
		helpstring("IWorldObject Interface"),
		pointer_default(unique)
	]
	interface IWorldObject : IDispatch
	{
		[propget, id(1), helpstring("property GUID")] HRESULT GUID([out, retval] long *pVal);
		[propget, id(3), helpstring("property Model")] HRESULT Model([out, retval] long *pVal);
		[propget, id(4), helpstring("property Icon")] HRESULT Icon([out, retval] long *pVal);
		[propget, id(5), helpstring("property Name")] HRESULT Name([out, retval] BSTR *pVal);
		[propget, id(6), helpstring("property Value")] HRESULT Value([out, retval] long *pVal);
		[propget, id(7), helpstring("property Container")] HRESULT Container([out, retval] long *pVal);
		[propget, id(8), helpstring("property Landblock")] HRESULT Landblock([out, retval] long *pVal);
		[propget, id(9), helpstring("property Offset")] HRESULT Offset([out] float *x, [out] float *y, [out] float *z, [out, retval] short *pVal);
		[propget, id(10), helpstring("property Heading")] HRESULT Heading([out] float *x, [out] float *y, [out] float *z, [out] float *w, [out, retval] short *pVal);
		[propget, id(11), helpstring("property Type")] HRESULT Type([out, retval] enum eObjectType *pVal);
		[propget, id(12), helpstring("property ItemSlots")] HRESULT ItemSlots([out, retval] long *pVal);
		[propget, id(13), helpstring("property PackSlots")] HRESULT PackSlots([out, retval] long *pVal);
		[propget, id(14), helpstring("property Lockable")] HRESULT Lockable([out, retval] VARIANT_BOOL *pVal);
		[propget, id(15), helpstring("property Inscribable")] HRESULT Inscribable([out, retval] VARIANT_BOOL *pVal);
		[propget, id(16), helpstring("property UsesLeft")] HRESULT UsesLeft([out, retval] long *pVal);
		[propget, id(17), helpstring("property TotalUses")] HRESULT TotalUses([out, retval] long *pVal);
		[propget, id(18), helpstring("property StackCount")] HRESULT StackCount([out, retval] long *pVal);
		[propget, id(19), helpstring("property StackMax")] HRESULT StackMax([out, retval] long *pVal);
		[propget, id(20), helpstring("property TradeNoteVendor")] HRESULT TradeNoteVendor([out, retval] long *pVal);
		[propget, id(21), helpstring("property AssociatedSpell")] HRESULT AssociatedSpell([out, retval] long *pVal);
		[propget, id(22), helpstring("property Slot")] HRESULT Slot([out, retval] long *pVal);
		[propget, id(23), helpstring("property Owner")] HRESULT Owner([out, retval] long *pVal);
		[propget, id(24), helpstring("property Wielder")] HRESULT Wielder([out, retval] long *pVal);
		[propget, id(25), helpstring("property WieldingSlot")] HRESULT WieldingSlot([out, retval] long *pVal);
		[propget, id(26), helpstring("property Coordinates")] HRESULT Coordinates([in, out] float *NorthSouth, [in, out] float *EastWest, [out, retval] VARIANT_BOOL *pVal);
		[propget, id(27), helpstring("property Distance")] HRESULT Distance([in, out] float *NorthSouth, [in, out] float *EastWest, [out, retval] VARIANT_BOOL *pVal);
		[propget, id(28), helpstring("property Monarch")] HRESULT Monarch([out, retval] long *pVal);
		[propget, id(29), helpstring("property Material")] HRESULT Material([out, retval] long *pVal);
		[propget, id(30), helpstring("property Coverage")] HRESULT Coverage([out, retval] long *pVal);
		[propget, id(31), helpstring("property Coverage2")] HRESULT Coverage2([out, retval] long *pVal);
		[propget, id(32), helpstring("property Coverage3")] HRESULT Coverage3([out, retval] long *pVal);
		[propget, id(33), helpstring("property EquipType")] HRESULT EquipType([out, retval] long *pVal);
		[propget, id(34), helpstring("property ApproachDistance")] HRESULT ApproachDistance([out, retval] float *pVal);
		[propget, id(35), helpstring("property SecondaryName")] HRESULT SecondaryName([out, retval] BSTR *pVal);
		[propget, id(36), helpstring("property RawCoordinates")] HRESULT RawCoordinates([in, out] float *pX, [in, out] float *pY, float *pZ, [out, retval] VARIANT_BOOL *pVal);
		[propget, id(37), helpstring("property Burden")] HRESULT Burden([out, retval] long *pVal);
		[propget, id(38), helpstring("property Workmanship")] HRESULT Workmanship([out, retval] float *pVal);
		[propget, id(39), helpstring("property IconOutline")] HRESULT IconOutline([out, retval] long *pVal);
		[propget, id(40), helpstring("property MissileType")] HRESULT MissileType([out, retval] long *pVal);
		[propget, id(41), helpstring("property TotalValue")] HRESULT TotalValue([out, retval] long *pVal);
		[propget, id(42), helpstring("property UsageMask")] HRESULT UsageMask([out, retval] long *pVal);
		[propget, id(43), helpstring("property HouseOwner")] HRESULT HouseOwner([out, retval] long *pVal);
		[propget, id(44), helpstring("property HookMask")] HRESULT HookMask([out, retval] long *pVal);
		[propget, id(45), helpstring("property HookType")] HRESULT HookType([out, retval] long *pVal);
		[propget, id(46), helpstring("property RealModel")] HRESULT RealModel([out, retval] long *pVal);
		[propget, id(47), helpstring("property Scale")] HRESULT Scale([out, retval] float* pVal);
		[propget, id(48), helpstring("property Flags")] HRESULT Flags([out, retval] long *pVal);
		[propget, id(49), helpstring("property CreateFlags1")] HRESULT CreateFlags1([out, retval] long *pVal);
		[propget, id(50), helpstring("property CreateFlags2")] HRESULT CreateFlags2([out, retval] long *pVal);
		[propget, id(51), helpstring("property ObjectFlags1")] HRESULT ObjectFlags1([out, retval] long *pVal);
		[propget, id(52), helpstring("property ObjectFlags2")] HRESULT ObjectFlags2([out, retval] long *pVal);
		[propget, id(53), helpstring("property HasIdData")]    HRESULT HasIdData([out, retval] VARIANT_BOOL *pVal);
		[propget, id(54), helpstring("property ArmorLevel")]   HRESULT ArmorLevel([out, retval] long *pVal);
		[propget, id(55), helpstring("property MagicDef")]     HRESULT MagicDef([out, retval] long *pVal);
		[propget, id(56), helpstring("property Spellcraft")]   HRESULT Spellcraft([out, retval] long *pVal);
		[propget, id(57), helpstring("property MaximumMana")]  HRESULT MaximumMana([out, retval] long *pVal);
		[propget, id(58), helpstring("property LoreReq")]      HRESULT LoreReq([out, retval] long *pVal);
		[propget, id(59), helpstring("property RankReq")]      HRESULT RankReq([out, retval] long *pVal);
		[propget, id(60), helpstring("property SkillReq")]     HRESULT SkillReq([out, retval] long *pVal);
		[propget, id(61), helpstring("property WieldReqType")] HRESULT WieldReqType([out, retval] long *pVal);
		[propget, id(62), helpstring("property WieldReqId")]   HRESULT WieldReqId([out, retval] long *pVal);
		[propget, id(63), helpstring("property WieldReq")]     HRESULT WieldReq([out, retval] long *pVal);
		[propget, id(64), helpstring("property TinkerCount")]  HRESULT TinkerCount([out, retval] long *pVal);
		[propget, id(65), helpstring("property SkillReqId")]   HRESULT SkillReqId([out, retval] long *pVal);
		[propget, id(66), helpstring("property SpecialProps")] HRESULT SpecialProps([out, retval] long *pVal);
		[propget, id(67), helpstring("property ManaCMod")]     HRESULT ManaCMod([out, retval] float *pVal);
		[propget, id(68), helpstring("property SpellCount")]   HRESULT SpellCount([out, retval] long *pVal);
		[propget, id(69), helpstring("property GetSpell")]     HRESULT Spell([in] long index, [out, retval] long *pVal);
		[propget, id(70), helpstring("property Inscription")]  HRESULT Inscription([out, retval] BSTR *pVal);
		[propget, id(71), helpstring("property WeapSpeed")]    HRESULT WeapSpeed([out, retval] long *pVal);
		[propget, id(72), helpstring("property EquipSkill")]   HRESULT EquipSkill([out, retval] long *pVal);
		[propget, id(73), helpstring("property DamageType")]   HRESULT DamageType([out, retval] long *pVal);
		[propget, id(74), helpstring("property MaxDamage")]    HRESULT MaxDamage([out, retval] long *pVal);
		[propget, id(75), helpstring("property Variance")]     HRESULT Variance([out, retval] float *pVal);
		[propget, id(76), helpstring("property DefenseBonus")] HRESULT DefenseBonus([out, retval] float *pVal);
		[propget, id(77), helpstring("property AttackBonus")]  HRESULT AttackBonus([out, retval] float *pVal);
		[propget, id(78), helpstring("property Range")]        HRESULT Range([out, retval] float *pVal);
		[propget, id(79), helpstring("property SlashProt")]    HRESULT SlashProt([out, retval] float *pVal);
		[propget, id(80), helpstring("property PierceProt")]   HRESULT PierceProt([out, retval] float *pVal);
		[propget, id(81), helpstring("property BludProt")]     HRESULT BludProt([out, retval] float *pVal);
		[propget, id(82), helpstring("property ColdProt")]     HRESULT ColdProt([out, retval] float *pVal);
		[propget, id(83), helpstring("property FireProt")]     HRESULT FireProt([out, retval] float *pVal);
		[propget, id(84), helpstring("property ElectProt")]    HRESULT ElectProt([out, retval] float *pVal);
		[propget, id(85), helpstring("property RaceReq")]      HRESULT RaceReq([out, retval] BSTR *pVal);
		[propget, id(86), helpstring("property DamageBonus")]  HRESULT DamageBonus([out, retval] float *pVal);
		[propget, id(87), helpstring("property AcidProt")]	   HRESULT AcidProt([out, retval] float *pVal);
	};

	[
		object,
		uuid(6D7556DA-19D1-4be3-BC18-357FEE292649),
		dual,
		helpstring("IWorldObject2 Interface"),
		pointer_default(unique)
	]
	interface IWorldObject2 : IUnknown
	{
		[propget, id(1), helpstring("property Flags")] HRESULT Flags([out, retval] long *pVal);
		[propget, id(2), helpstring("property CreateFlags1")] HRESULT CreateFlags1([out, retval] long *pVal);
		[propget, id(3), helpstring("property CreateFlags2")] HRESULT CreateFlags2([out, retval] long *pVal);
		[propget, id(4), helpstring("property ObjectFlags1")] HRESULT ObjectFlags1([out, retval] long *pVal);
		[propget, id(5), helpstring("property ObjectFlags2")] HRESULT ObjectFlags2([out, retval] long *pVal);
	};

	[
		uuid(175F99F1-B937-47C5-846E-CACBB986AFD9),
		helpstring("IWorldEvents Interface")
	]
	dispinterface IWorldEvents
	{
		properties:
		methods:
		[id(1), helpstring("method CreateObject")] HRESULT CreateObject(IWorldObject *pObject);
		[id(2), helpstring("method ReleaseObject")] HRESULT ReleaseObject(IWorldObject *pObject);
		[id(3), helpstring("method ChangeObject")] HRESULT ChangeObject(IWorldObject *pObject, BSTR Change);
		[id(4), helpstring("method MovedObject")] HRESULT MoveObject(IWorldObject *pObject);
		[id(5), helpstring("method ReleaseDone")] HRESULT ReleaseDone();
	};

	[
		object,
		uuid(6FC29250-9FB4-4A0C-AF16-329E35DF02F2),
		dual,
		helpstring("IWorldIterator Interface"),
		pointer_default(unique)
	]
	interface IWorldIterator : IDispatch
	{
		[propget, hidden, id(1), helpstring("property Next_Old")] HRESULT Next_Old([out, retval] IWorldObject * *pVal);
		[id(2), helpstring("Resets the iterator back to its starting position")] HRESULT Reset();
		[propget, id(3), helpstring("The number of stacks in the iterator")] HRESULT Count([out, retval] long *pVal);
		[propget, id(4), helpstring("The next object in the iterator")] HRESULT Next([in, out] IWorldObject **ppObject, [out, retval] VARIANT_BOOL *pVal);
		[id(5), helpstring("Removes the last search criteria object from the list")] HRESULT Pop();
		[id(6), helpstring("method ByName")] HRESULT ByName(BSTR strName);
		[id(7), helpstring("method ByNameSubstring")] HRESULT ByNameSubstring(BSTR strSubstring);
		[id(8), helpstring("method ByType")] HRESULT ByType(enum eObjectType Type);
		[id(9), helpstring("method ByAll")] HRESULT ByAll();
		[id(10), helpstring("method ByInventory")] HRESULT ByInventory();
		[id(11), helpstring("method ByContainer")] HRESULT ByContainer(long nContainer);
		[propget, id(12), helpstring("The number of items in this iterator")] HRESULT Quantity([out, retval] long *pVal);
		[id(13), helpstring("method ByOwner")] HRESULT ByOwner(long nOwner);
		[id(14), helpstring("method ByLandscape")] HRESULT ByLandscape();
	};
	
	[
		uuid(53092D1B-F0B0-46FF-BF11-8F031EC9B137),
		helpstring("World Class")
	]
	coclass World
	{
		[default] interface IWorld;
		[default, source] dispinterface IWorldEvents;
	};

	[
		uuid(50A7E9EC-AB12-4484-9C28-C2A39274A636),
		helpstring("WorldObject Class")
	]
	coclass WorldObject
	{
		[default] interface IWorldObject;
	};

	[
		uuid(A34E27C6-0403-4027-908D-BABF23919E83),
		helpstring("WorldObject2 Class")
	]
	coclass WorldObject2
	{
		[default] interface IWorldObject2;
	};

	[
		uuid(2681B113-294E-4ABF-B543-624194846BE1),
		helpstring("WorldIterator Class")
	]
	coclass WorldIterator
	{
		[default] interface IWorldIterator;
	};

	[
		uuid(B0C05A9A-273D-4BC4-B7BA-E27CF94C8EF4),
		helpstring("IdentifyQueue Class")
	]
	coclass IdentifyQueue
	{
		[default] interface IIdentifyQueue;
	};
};

