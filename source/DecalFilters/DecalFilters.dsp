# Microsoft Developer Studio Project File - Name="DecalFilters" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=DecalFilters - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "DecalFilters.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "DecalFilters.mak" CFG="DecalFilters - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "DecalFilters - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "DecalFilters - Win32 Release MinDependency" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "DecalFilters - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GX /ZI /Od /I "..\Include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /FR /Yu"stdafx.h" /Ge /FD /GZ /c
# ADD MTL /nologo /I "..\Include" /Oicf
# ADD BASE RSC /l 0x1009 /d "_DEBUG"
# ADD RSC /l 0x1009 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386
# Begin Custom Build - Performing registration
OutDir=.\..\Debug
TargetPath=\mysource\Debug\DecalFilters.dll
InputPath=\mysource\Debug\DecalFilters.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ELSEIF  "$(CFG)" == "DecalFilters - Win32 Release MinDependency"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "ReleaseMinDependency"
# PROP BASE Intermediate_Dir "ReleaseMinDependency"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Release"
# PROP Intermediate_Dir "ReleaseMinDependency"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /O1 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "_ATL_STATIC_REGISTRY" /D "_ATL_MIN_CRT" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /Zi /Oa /Og /Oi /Os /Oy /Ob1 /Gf /Gy /I "..\Include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /FR /Yu"stdafx.h" /FD /c
# ADD MTL /nologo /I "..\Include" /Oicf
# ADD BASE RSC /l 0x1009 /d "NDEBUG"
# ADD RSC /l 0x1009 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib winmm.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept /libpath:"..\Release"
# Begin Custom Build - Performing registration
OutDir=.\..\Release
TargetPath=\mysource\Release\DecalFilters.dll
InputPath=\mysource\Release\DecalFilters.dll
SOURCE="$(InputPath)"

"$(OutDir)\regsvr32.trg" : $(SOURCE) "$(INTDIR)" "$(OUTDIR)"
	regsvr32 /s /c "$(TargetPath)" 
	echo regsvr32 exec. time > "$(OutDir)\regsvr32.trg" 
	
# End Custom Build

!ENDIF 

# Begin Target

# Name "DecalFilters - Win32 Debug"
# Name "DecalFilters - Win32 Release MinDependency"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AllegianceInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\AttributeInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\CharacterStats.cpp
# End Source File
# Begin Source File

SOURCE=.\DecalFilters.cpp
# End Source File
# Begin Source File

SOURCE=.\DecalFilters.def
# End Source File
# Begin Source File

SOURCE=.\DecalFilters.idl
# ADD MTL /tlb ".\DecalFilters.tlb" /h "DecalFilters.h" /iid "DecalFilters_i.c" /Oicf
# End Source File
# Begin Source File

SOURCE=.\DecalFilters.rc
# End Source File
# Begin Source File

SOURCE=.\EchoFilter.cpp
# End Source File
# Begin Source File

SOURCE=.\EchoFilter2.cpp
# End Source File
# Begin Source File

SOURCE=.\Enchantment.cpp
# End Source File
# Begin Source File

SOURCE=.\IdentifyQueue.cpp
# End Source File
# Begin Source File

SOURCE=.\Prefilter.cpp
# End Source File
# Begin Source File

SOURCE=.\SkillInfo.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\World.cpp
# End Source File
# Begin Source File

SOURCE=.\WorldIterator.cpp
# End Source File
# Begin Source File

SOURCE=.\WorldObject.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AllegianceInfo.h
# End Source File
# Begin Source File

SOURCE=.\AttributeInfo.h
# End Source File
# Begin Source File

SOURCE=.\CharacterStats.h
# End Source File
# Begin Source File

SOURCE=.\DecalFiltersCP.h
# End Source File
# Begin Source File

SOURCE=.\EchoFilter.h
# End Source File
# Begin Source File

SOURCE=.\EchoFilter2.h
# End Source File
# Begin Source File

SOURCE=.\Enchantment.h
# End Source File
# Begin Source File

SOURCE=.\FilterImpl.h
# End Source File
# Begin Source File


SOURCE=.\IdentifyQueue.h
# End Source File
# Begin Source File

SOURCE=.\Prefilter.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SkillInfo.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\World.h
# End Source File
# Begin Source File

SOURCE=.\WorldIterator.h
# End Source File
# Begin Source File

SOURCE=.\WorldObject.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\AllegianceInfo.rgs
# End Source File
# Begin Source File

SOURCE=.\CharacterStats.rgs
# End Source File
# Begin Source File

SOURCE=.\EchoFilter.rgs
# End Source File
# Begin Source File

SOURCE=.\EchoFilter2.rgs
# End Source File
# Begin Source File

SOURCE=.\Enchantment.rgs
# End Source File
# Begin Source File

SOURCE=.\IdentifyQueue.rgs
# End Source File
# Begin Source File

SOURCE=.\Prefilter.rgs
# End Source File
# Begin Source File

SOURCE=.\World.rgs
# End Source File
# Begin Source File

SOURCE=.\WorldIterator.rgs
# End Source File
# Begin Source File

SOURCE=.\WorldObject.rgs
# End Source File
# End Group
# End Target
# End Project
