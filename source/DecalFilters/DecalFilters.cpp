// DecalFilters.cpp : Implementation of DLL Exports.


// Note: Proxy/Stub Information
//      To build a separate proxy/stub DLL, 
//      run nmake -f DecalFiltersps.mk in the project directory.

#include "stdafx.h"
#include "resource.h"
#include <initguid.h>
#include "DecalFilters.h"

#include "EchoFilter.h"
#include "EchoFilter2.h"
#include "Prefilter.h"
#include "CharacterStats.h"

#include "DecalFilters_i.c"
#include <DecalNet_i.c>

#include "World.h"
#include "WorldObject.h"
#include "WorldIterator.h"

#include "IdentifyQueue.h"

CComModule _Module;

BEGIN_OBJECT_MAP(ObjectMap)
   OBJECT_ENTRY(CLSID_EchoFilter, cEchoFilter)
   OBJECT_ENTRY(CLSID_EchoFilter2, cEchoFilter2)
   OBJECT_ENTRY(CLSID_Prefilter, cPrefilter)
   OBJECT_ENTRY(CLSID_CharacterStats, cCharacterStats)
   OBJECT_ENTRY(CLSID_World, cWorld)
   OBJECT_ENTRY(CLSID_WorldObject, cWorldObject)
   OBJECT_ENTRY(CLSID_WorldIterator, cWorldIterator)
   OBJECT_ENTRY(CLSID_IdentifyQueue, CIdentifyQueue)
END_OBJECT_MAP()

/////////////////////////////////////////////////////////////////////////////
// DLL Entry Point

extern "C"
BOOL WINAPI DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
    if (dwReason == DLL_PROCESS_ATTACH)
    {
        _Module.Init(ObjectMap, hInstance, &LIBID_DecalFilters);
        DisableThreadLibraryCalls(hInstance);
    }
    else if (dwReason == DLL_PROCESS_DETACH)
        _Module.Term();

    return TRUE;    // ok
}

/////////////////////////////////////////////////////////////////////////////
// Used to determine whether the DLL can be unloaded by OLE

STDAPI DllCanUnloadNow(void)
{
    return (_Module.GetLockCount()==0) ? S_OK : S_FALSE;
}

/////////////////////////////////////////////////////////////////////////////
// Returns a class factory to create an object of the requested type

STDAPI DllGetClassObject(REFCLSID rclsid, REFIID riid, LPVOID* ppv)
{
    return _Module.GetClassObject(rclsid, riid, ppv);
}

/////////////////////////////////////////////////////////////////////////////
// DllRegisterServer - Adds entries to the system registry

STDAPI DllRegisterServer(void)
{
    // registers object, typelib and all interfaces in typelib
    return _Module.RegisterServer(TRUE);
}

/////////////////////////////////////////////////////////////////////////////
// DllUnregisterServer - Removes entries from the system registry

STDAPI DllUnregisterServer(void)
{
    return _Module.UnregisterServer(TRUE);
}