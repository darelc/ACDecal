// SkillInfo.cpp : Implementation of cSkillInfo
#include "stdafx.h"
#include "DecalFilters.h"
#include "SkillInfo.h"

/////////////////////////////////////////////////////////////////////////////
// cSkillInfo


STDMETHODIMP cSkillInfo::get_Name(BSTR *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   USES_CONVERSION;
   *pVal = T2BSTR( m_pSkill->m_szName.c_str() );

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_ShortName(BSTR *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   TCHAR *szShortName = new TCHAR[ ::_tcslen( m_pSkill->m_szName.c_str() ) + 1 ];
   for( TCHAR *i_short = szShortName, *i_long = const_cast< char * >( m_pSkill->m_szName.c_str() ); *i_long != _T( '\0' ); ++ i_long )
   {
      if( *i_long != _T( ' ' ) )
         *( i_short ++ ) = *i_long;
   }

   *i_short = _T( '\0' );

   USES_CONVERSION;
   *pVal = T2BSTR( szShortName );

   delete[] szShortName;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Formula(BSTR *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   TCHAR szBuffer[ 255 ];
   if( m_pSkill->m_nAttribute2 != eAttrNULL )
   {
	   LPSTR attrA, attrB;
	   switch(m_pSkill->m_nAttribute1)
	   {
	   case eAttrStrength: attrA = "Strength"; break;
	   case eAttrEndurance: attrA = "Endurance"; break;
	   case eAttrCoordination: attrA = "Coordination"; break;
	   case eAttrQuickness: attrA = "Quickness"; break;
	   case eAttrFocus: attrA = "Focus"; break;
	   case eAttrSelf: attrA = "Self"; break;
	   }
	   switch(m_pSkill->m_nAttribute2)
	   {
	   case eAttrStrength: attrB = "Strength"; break;
	   case eAttrEndurance: attrB = "Endurance"; break;
	   case eAttrCoordination: attrB = "Coordination"; break;
	   case eAttrQuickness: attrB = "Quickness"; break;
	   case eAttrFocus: attrB = "Focus"; break;
	   case eAttrSelf: attrB = "Self"; break;
	   }
      ::_stprintf( szBuffer, _T( "( %s + %s ) / %i" ), attrA, attrB, m_pSkill->m_nDenominator );
   }
   else if( m_pSkill->m_nAttribute1 != eAttrNULL )
   {
	   LPSTR attrA;
	   switch(m_pSkill->m_nAttribute1)
	   {
	   case eAttrStrength: attrA = "Strength"; break;
	   case eAttrEndurance: attrA = "Endurance"; break;
	   case eAttrCoordination: attrA = "Coordination"; break;
	   case eAttrQuickness: attrA = "Quickness"; break;
	   case eAttrFocus: attrA = "Focus"; break;
	   case eAttrSelf: attrA = "Self"; break;
	   }
      ::_stprintf( szBuffer, _T( "( %s ) / %i" ), attrA, m_pSkill->m_nDenominator );
   }
   else
      ::strcpy( szBuffer, _T( "Unknown" ) );

   USES_CONVERSION;
   *pVal = T2BSTR( szBuffer );

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Base(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   float fSkill = 0.0;
   if( m_pSkill->m_nAttribute1 != eAttrNULL )
      fSkill += m_pStats->PrimStat[ m_pSkill->m_nAttribute1 - 1];
   if( m_pSkill->m_nAttribute2 != eAttrNULL )
      fSkill += m_pStats->PrimStat[ m_pSkill->m_nAttribute2 - 1];
 
   fSkill /= static_cast< float >( m_pSkill->m_nDenominator );
   fSkill += 0.5f;
  
   *pVal = static_cast< long >( fSkill );

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Current(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   long nBase;
   get_Base( &nBase );

   *pVal = nBase + m_pSkill->m_nOffset + m_pSkill->m_nBonus;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Increment(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pSkill->m_nOffset;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Exp(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pSkill->m_nExp;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Training(enum eTrainingType *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pSkill->m_trained;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Known(VARIANT_BOOL *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = ( ::_tcscmp( m_pSkill->m_szName.c_str(), _T( "Unknown" ) ) == 0 ) ? VARIANT_FALSE : VARIANT_TRUE;

	return S_OK;
}

STDMETHODIMP cSkillInfo::get_Bonus(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = m_pSkill->m_nBonus;

	return S_OK;
}