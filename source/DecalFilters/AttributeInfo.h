// AttributeInfo.h : Declaration of the cAttributeInfo

#ifndef __ATTRIBUTEINFO_H_
#define __ATTRIBUTEINFO_H_

#include "resource.h"       // main symbols
#include "CharacterStats.h"

/////////////////////////////////////////////////////////////////////////////
// cAttributeInfo
class ATL_NO_VTABLE cAttributeInfo : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cAttributeInfo, &CLSID_AttributeInfo>,
	public IDispatchImpl<IAttributeInfo, &IID_IAttributeInfo, &LIBID_DecalFilters>
{
public:
	cAttributeInfo()
	{
		m_pInfo = &m_pAttribInfo;
	}

   cCharacterStats::cAttributeInfo m_pAttribInfo;
   cCharacterStats::cAttributeInfo *m_pInfo;

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cAttributeInfo)
	COM_INTERFACE_ENTRY(IAttributeInfo)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IAttributeInfo
public:
	STDMETHOD(get_Current)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Exp)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Creation)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_Name)(/*[out, retval]*/ BSTR *pVal);
};

#endif //__ATTRIBUTEINFO_H_
