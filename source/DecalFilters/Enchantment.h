// Enchantment.h: Definition of the Enchantment class
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ENCHANTMENT_H__C2D64F56_5FD0_43CD_9E1D_C9981D1F98C5__INCLUDED_)
#define AFX_ENCHANTMENT_H__C2D64F56_5FD0_43CD_9E1D_C9981D1F98C5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "resource.h"       // main symbols
#include "CharacterStats.h"

/////////////////////////////////////////////////////////////////////////////
// Enchantment

class Enchantment : 
	public IDispatchImpl<IEnchantment, &IID_IEnchantment, &LIBID_DecalFilters>, 
	public CComObjectRoot,
	public CComCoClass<Enchantment,&CLSID_Enchantment>
{
public:
	Enchantment() {}

BEGIN_COM_MAP(Enchantment)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(IEnchantment)
END_COM_MAP()

//DECLARE_REGISTRY_RESOURCEID(IDR_Enchantment)

private:
	long SpellID, Layer, SecondsLeft, Affected, AffectMask, Family;
	double Adjustment;


// IEnchantment
public:
	STDMETHOD(get_Adjustment)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_Adjustment)(/*[in]*/ double newVal);
	STDMETHOD(get_Family)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Family)(/*[in]*/ long newVal);
	STDMETHOD(get_AffectedMask)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_AffectedMask)(/*[in]*/ long newVal);
	STDMETHOD(get_Affected)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Affected)(/*[in]*/ long newVal);
	STDMETHOD(get_TimeRemaining)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_TimeRemaining)(/*[in]*/ long newVal);
	STDMETHOD(get_Layer)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Layer)(/*[in]*/ long newVal);
	STDMETHOD(get_SpellID)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_SpellID)(/*[in]*/ long newVal);
};

#endif // !defined(AFX_ENCHANTMENT_H__C2D64F56_5FD0_43CD_9E1D_C9981D1F98C5__INCLUDED_)
