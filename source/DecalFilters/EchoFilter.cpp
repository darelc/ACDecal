// EchoFilter.cpp : Implementation of cEchoFilter
#include "stdafx.h"
#include "DecalFilters.h"
#include "EchoFilter.h"

/////////////////////////////////////////////////////////////////////////////
// cEchoFilter


STDMETHODIMP cEchoFilter::Dispatch(IMessage *pMsg)
{
   Fire_EchoMessage( pMsg );

	return S_OK;
}
