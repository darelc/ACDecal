
DecalDatps.dll: dlldata.obj DecalDat_p.obj DecalDat_i.obj
	link /dll /out:DecalDatps.dll /def:DecalDatps.def /entry:DllMain dlldata.obj DecalDat_p.obj DecalDat_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del DecalDatps.dll
	@del DecalDatps.lib
	@del DecalDatps.exp
	@del dlldata.obj
	@del DecalDat_p.obj
	@del DecalDat_i.obj
