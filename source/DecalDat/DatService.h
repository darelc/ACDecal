// DatService.h : Declaration of the cDatService

#ifndef __DATSERVICE_H_
#define __DATSERVICE_H_

#include "resource.h"       // main symbols
#include <DecalImpl.h>

class cDatLibrary;

/////////////////////////////////////////////////////////////////////////////
// cDatService
class ATL_NO_VTABLE cDatService : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cDatService, &CLSID_DatService>,
   public IDecalServiceImpl< cDatService >,
   public IDecalDirectory,
	public IDispatchImpl<IDatService, &IID_IDatService, &LIBID_DecalDat>
{
public:
	cDatService()
      : m_pCell( NULL ),
      m_pPortal( NULL )
	{
	}

   struct cFileFilter
   {
      std::string m_strName;
      CLSID m_clsid;
      bool m_bCache;
   };

   typedef std::vector< cFileFilter > cFileFilterList;
   cFileFilterList m_filters;

   cFileFilter *getFilter( LPTSTR szFilter, int nLength );
   HRESULT createFilter( cFileFilter *pFilter, REFIID iid, LPVOID *ppvItf );

   enum eLibrary
   {
      eCell,
      ePortal
   };

   struct cFileCache
   {
      cFileFilter *m_pFilter;
      eLibrary m_library;
      DWORD m_dwFile;
      CComPtr< IUnknown > m_pFile;
   };

   typedef std::deque< cFileCache > cFileCacheList;
   cFileCacheList m_cache;

   HRESULT onInitialize();
   void onTerminate();

   cDatLibrary *m_pCell,
      *m_pPortal;

   static cDatService *g_p;

   // Hooked functions
   typedef HANDLE (WINAPI *fn_CreateFile)(LPCTSTR, DWORD, DWORD, LPSECURITY_ATTRIBUTES, DWORD, DWORD, HANDLE);
   static fn_CreateFile g_fn_CreateFile;

DECLARE_REGISTRY_RESOURCEID(IDR_DATSERVICE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDatService)
   COM_INTERFACE_ENTRY(IDecalService)
   COM_INTERFACE_ENTRY(IDecalDirectory)
	COM_INTERFACE_ENTRY(IDatService)
	COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

// IDatService
public:

   // IDecalDirectory
   STDMETHOD(Lookup)(BSTR strName, IUnknown **ppItf);
};

#endif //__DATSERVICE_H_
