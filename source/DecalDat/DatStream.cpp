// DatStream.cpp : Implementation of cDatStream
#include "stdafx.h"
#include "DecalDat.h"
#include "DatStream.h"

/////////////////////////////////////////////////////////////////////////////
// cDatStream


STDMETHODIMP cDatStream::get_Size(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = static_cast< long >( m_pFile->getSize() );

	return S_OK;
}

STDMETHODIMP cDatStream::get_Tell(long *pVal)
{
   if( pVal == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   *pVal = static_cast< long >( m_pFile->tell() );

	return S_OK;
}

STDMETHODIMP cDatStream::Skip(long Bytes)
{
   if( Bytes < 0 )
   {
      _ASSERT( FALSE );
      return E_INVALIDARG;
   }

   m_pFile->skip( static_cast< DWORD >( Bytes ) );

	return S_OK;
}

STDMETHODIMP cDatStream::Restart()
{
   m_pFile->reset();

	return S_OK;
}

STDMETHODIMP cDatStream::ReadBinary(long Bytes, BYTE *Buffer)
{
   if( Bytes < 0 )
   {
      _ASSERT( FALSE );
      return E_INVALIDARG;
   }

   if( Buffer == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   m_pFile->read( Buffer, static_cast< DWORD >( Bytes ) );

	return S_OK;
}

STDMETHODIMP cDatStream::Read(long Bytes, BSTR *Data)
{
   if( Bytes < 0 )
   {
      _ASSERT( FALSE );
      return E_INVALIDARG;
   }

   if( Data == NULL )
   {
      _ASSERT( FALSE );
      return E_POINTER;
   }

   BYTE *Buffer = reinterpret_cast< BYTE * >( ::_alloca( Bytes ) );
   m_pFile->read( Buffer, static_cast< DWORD >( Bytes ) );

   *Data = ::SysAllocStringByteLen( reinterpret_cast< LPCTSTR >( Buffer ), Bytes );

	return S_OK;
}
