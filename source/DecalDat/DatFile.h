// DatFile.h
// Declaration of class cDatFile
// Class for extracting data from portal.dat and cell.dat

#ifndef __ACFILE_H
#define __ACFILE_H

#define FILE_COUNT   62

class cDatFile
{
   // The AC File is created as a memory mapped file
   // so we let the memory manager cache manage - it's also quite a bit nicer than
   // code to seek/read everything
   HANDLE m_hFile,
      m_hMapping;
   BYTE *m_pData;
   DWORD m_dwSectorSize;

public:
   class cFile
   {
      BYTE *m_pFirstSector,
         *m_pCurrentSector,
         *m_pCurrentByte;
      DWORD m_dwSize,
         m_dwOffset;
      cDatFile *m_pSource;

   public:
      // Locate the file in a directory

      cFile( cDatFile *pSource, BYTE *pFirstSector, DWORD dwSize );

      DWORD getSize() const
      {
         return m_dwSize;
      }

      DWORD tell() const
      {
         return m_dwOffset;
      }

      void reset();
      DWORD read( BYTE *pbBuffer, DWORD dwSize );
      DWORD skip( DWORD dwSize );
   };

   cDatFile( LPCTSTR szFilename, DWORD dwSectorSize = 256 );
   ~cDatFile();

   cFile getFile( DWORD dwFileNumber );

   // Structures
#pragma pack( push, 1 )
   struct cFileEntry
   {
      DWORD m_dwID,
         m_dwOffset,
         m_dwSize;
   };

   struct cDirectory
   {
      DWORD m_subdirs[ FILE_COUNT ];
      DWORD m_dwFiles;
      cFileEntry m_files[ FILE_COUNT ];
   };

#pragma pack( pop )

   friend cFile;
};

#endif
