// PluginSite.h : Declaration of the cPluginSite

#ifndef __PLUGINSITE_H_
#define __PLUGINSITE_H_

#include "resource.h"       // main symbols

class cDecal;

/////////////////////////////////////////////////////////////////////////////
// cPluginSite
class ATL_NO_VTABLE cPluginSite : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cPluginSite, &CLSID_PluginSite2>,
	public IPluginSite2
{
public:
	cPluginSite()
	{
	}

   void FinalRelease();

DECLARE_REGISTRY_RESOURCEID(IDR_PLUGINSITE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPluginSite)
	COM_INTERFACE_ENTRY(IPluginSite2)
END_COM_MAP()

   CComPtr< IPlugin2 > m_pPlugin;
   cDecal *m_pDecal;

// IPluginSite2
public:
	STDMETHOD(get_Object)(BSTR Path, /*[out, retval]*/ LPDISPATCH *pVal);
	STDMETHOD(get_Decal)(/*[out, retval]*/ IDecal * *pVal);
	STDMETHOD(Unload)();
};

#endif //__PLUGINSITE_H_
