// SurrogateRemove.h : Declaration of the cSurrogateRemove

#ifndef __SURROGATEREMOVE_H_
#define __SURROGATEREMOVE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cSurrogateRemove
class ATL_NO_VTABLE cSurrogateRemove : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<cSurrogateRemove, &CLSID_SurrogateRemove>,
   public IDecalUninstall
{
public:
	cSurrogateRemove()
	{
	}

   CComBSTR m_strGroup;
   CLSID m_clsid;
   _bstr_t m_strProgID;

DECLARE_REGISTRY_RESOURCEID(IDR_SURROGATEREMOVE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cSurrogateRemove)
   COM_INTERFACE_ENTRY(IDecalUninstall)
END_COM_MAP()

// ISurrogateRemove
public:

   // IDecalUninstall
   STDMETHOD(Prepare)(IDecalEnum *pEnum)
   {
      pEnum->get_Group( &m_strGroup );
      pEnum->get_ComClass( &m_clsid );

      CComVariant vProgID;
      pEnum->get_Property( _bstr_t( _T( "ProgID" ) ), &vProgID );
      if( vProgID.vt == VT_BSTR )
         m_strProgID = vProgID.bstrVal;

      return S_OK;
   }

   STDMETHOD(Uninstall)()
   {
      USES_CONVERSION;
      TCHAR szParent[ 255 ];
      ::_tcscat ( ::_tcscpy ( szParent, _T( "Software\\Decal\\" ) ), OLE2T( m_strGroup ) );

      RegKey rk;
      if( rk.Open( HKEY_LOCAL_MACHINE, szParent ) != ERROR_SUCCESS )
      {
         _ASSERT( FALSE );
         return E_FAIL;
      }

      LPOLESTR strCLSID;
      ::StringFromCLSID( m_clsid, &strCLSID );

      LPTSTR szCLSID = OLE2T( strCLSID );

      ::CoTaskMemFree( strCLSID );

      if( rk.RecurseDeleteKey( szCLSID ) != ERROR_SUCCESS )
      {
         _ASSERT( FALSE );
         return E_FAIL;
      }

      if( m_strProgID.length() > 0 )
      {
         LPTSTR szProgID = OLE2T( m_strProgID );
         RegKey rkcls;
         rkcls.Open( HKEY_CLASSES_ROOT, NULL );
         rkcls.RecurseDeleteKey( szProgID );
      }

      return S_OK;
   }
};

#endif //__SURROGATEREMOVE_H_
