//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Decal.rc
//
#define IDS_PROJNAME                    100
#define IDR_DECAL                       101
#define IDR_PLUGINSITE                  102
#define IDR_DECALENUM                   103
#define IDR_SURROGATEREMOVE             104
#define IDR_ACTIVEXSURROGATE            105
#define IDR_DECALRES                    106
#define IDR_DECALNODE                   107
#define IDS_ACHOOKS_DESC                111
#define IDR_ACHooks                     112
#define IDE_INDEXOUTOFRANGE2            512
#define IDE_INDEXOUTOFRANGE             513
#define IDE_BADINDEXTYPE                514
#define IDE_CLASSNOTFOUND               515
#define IDE_CLASSKEYNOTFOUND            516
#define IDE_GROUPKEYNOTOPEN             517
#define IDE_INVALIDSUBKEYS              518
#define IDE_BADITERATOR                 519
#define IDE_MOVEUPTOP                   520
#define IDE_MOVEDOWNBOTTOM              521
#define IDE_SURROGATEDISABLED           522

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           113
#endif
#endif
