// ActiveXSurrogate.h : Declaration of the cActiveXSurrogate

#ifndef __ACTIVEXSURROGATE_H_
#define __ACTIVEXSURROGATE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cActiveXSurrogate
class ATL_NO_VTABLE cActiveXSurrogate : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cActiveXSurrogate, &CLSID_ActiveXSurrogate>,
   public IDecalFileSurrogate,
   public IDecalUninstall
{
public:
	cActiveXSurrogate()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ACTIVEXSURROGATE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cActiveXSurrogate)
   COM_INTERFACE_ENTRY(IDecalFileSurrogate)
   COM_INTERFACE_ENTRY(IDecalUninstall)
END_COM_MAP()

   CComBSTR m_strFile;
   CComBSTR m_strGroup;
   CLSID m_clsid;

public:
   // IDecalFileSurrogate
   STDMETHOD(get_Extension)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "dll" ) );
      return S_OK;
   }

   STDMETHOD(get_Description)( BSTR *pVal )
   {
      if( pVal == NULL )
      {
         _ASSERT( FALSE );
         return E_POINTER;
      }

      *pVal = T2BSTR( _T( "ActiveX Plugin" ) );
      return S_OK;
   }

   STDMETHOD(Register)(BSTR strFilename);

   // IDecalUninstall
   STDMETHOD(Prepare)( IDecalEnum *pEnum )
   {
      // Get the class
      pEnum->get_ComClass( &m_clsid );

      // Get the file
      static _bstr_t _strFile( _T( "File" ) );

      CComVariant vFile;
      pEnum->get_Property( _strFile, &vFile );
      m_strFile = vFile.bstrVal;

      // Get the group
      pEnum->get_Group( &m_strGroup );

      return S_OK;
   }

   STDMETHOD(Uninstall)();
};

#endif //__ACTIVEXSURROGATE_H_
