// DecalEnum.h : Declaration of the cDecalEnum

#ifndef __DECALENUM_H_
#define __DECALENUM_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cDecalEnum
class ATL_NO_VTABLE cDecalEnum : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public CComCoClass<cDecalEnum, &CLSID_DecalEnum>,
	public IDecalEnum
{
public:
	cDecalEnum()
      : m_nIndex( -1 )
	{
	}

   ~cDecalEnum();

DECLARE_REGISTRY_RESOURCEID(IDR_DECALENUM)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cDecalEnum)
	COM_INTERFACE_ENTRY(IDecalEnum)
END_COM_MAP()

   bool Initialize( IDecal *pDecal, BSTR strKey );

   // Skip to a single key - next will terminate iteration
   HRESULT Advance( REFCLSID clsid );
   // Load an ordered list of keys
   HRESULT Begin();

   struct cKeyEntry
   {
      CLSID m_clsid;
      TCHAR m_letter;
   };

   void insertKey ( cKeyEntry &ke, TCHAR *szOrder, TCHAR *szEndOrder );
   HRESULT openKeyAt ( int nIndex );

   typedef std::vector< cKeyEntry > cKeyList;
   cKeyList m_keys;

   int m_nIndex;
   _bstr_t m_strGroup;
   std::string m_strGroupKey;
   RegKey m_key;
   bool m_bOrderChanged;

   // This member is purely for holdinga reference to the main decal object
   // and keep this object from being orphaned
   CComPtr< IDecal > m_pDecal;

// IDecalEnum
public:
	STDMETHOD(MoveBefore)(REFCLSID clsidBefore);
	STDMETHOD(get_Group)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_Property)(BSTR Name, /*[out, retval]*/ VARIANT *pVal);
	STDMETHOD(get_ResourcePath)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(get_SurrogateClass)(/*[out, retval]*/ CLSID *pVal);
	STDMETHOD(Next)();
	STDMETHOD(CreateInstance)(REFIID iid, /*[out, retval, iid_is(iid)]*/ LPVOID *ppvItf);
	STDMETHOD(get_Enabled)(/*[out, retval]*/ VARIANT_BOOL *pVal);
	STDMETHOD(put_Enabled)(/*[in]*/ VARIANT_BOOL newVal);
	STDMETHOD(get_ComClass)(/*[out, retval]*/ CLSID *pVal);
	STDMETHOD(get_FriendlyName)(/*[out, retval]*/ BSTR *pVal);
   STDMETHOD(Skip)(REFCLSID clsid);
};

#endif //__DECALENUM_H_
