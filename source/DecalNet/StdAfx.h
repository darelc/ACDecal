// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__88F425A2_1F36_4573_B1E2_C32EF26B2CBA__INCLUDED_)
#define AFX_STDAFX_H__88F425A2_1F36_4573_B1E2_C32EF26B2CBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#define _WIN32_WINDOWS 0x0410
#define _ATL_APARTMENT_THREADED

#pragma warning(disable:4530)

//C Library includes
#include <stdio.h>
#include <stdlib.h>

#include <atlbase.h>
#include <atlcrypt.h>

//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
#include <atlcom.h>

#include <Decal.h>
#include <time.h>

#include <list>
#include <string>
#include <map>
#include <deque>
#include <vector>
#include "../Include/VSBridge.h"

#import <msxml.dll>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__88F425A2_1F36_4573_B1E2_C32EF26B2CBA__INCLUDED)
