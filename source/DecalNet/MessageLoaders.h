// MessageLoaders.h
// Declaration of Primitive Data Type loaders for encoded message data

#ifndef __MESSAGELOADERS_H
#define __MESSAGELOADERS_H

class cFieldLoader
{
public:
   virtual void *skip(void *) = 0;
   virtual void *align(void *pData, void *pStart) = 0;
   virtual bool testValue(void *pData, void *pEnd) = 0;
   virtual void getValue( void *, LPVARIANT pDest ) = 0;
   virtual long getNumber( void *pvData );

   static cFieldLoader *lookup( const _bstr_t &strName );

   static void init();
   static void term();

private:
   static void addLoader( LPCTSTR szName, cFieldLoader *pLoader );

   typedef std::map< _bstr_t, VSBridge::auto_ptr< cFieldLoader > > cFieldLoaderMap;
   static cFieldLoaderMap g_primitives;
};

#endif
