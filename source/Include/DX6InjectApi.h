// DX6InjectApi.h
// Declaration of exported functions from DX6Inject

#ifndef __DX6INJECTAPI_H
#define __DX6INJECTAPI_H

#ifdef DX6INJECT_IMPL
#define DX6INJECT_API   __declspec(dllexport)
#else
#define DX6INJECT_API   __declspec(dllimport)
#endif

// Adds a reference to the registered hook function
void DX6INJECT_API InjectInstall();

// Removes a reference to the registered hook function
void DX6INJECT_API InjectUninstall();

LRESULT CALLBACK DX6Callback( int, WPARAM, LPARAM );

#endif
