// DecalInputImpl.h
// Declaration of classes for inputing Input Actions

#ifndef __DECALINPUTIMPL_H
#define __DECALINPUTIMPL_H

#include "DecalInput.h"

template< class ImplT >
class IInputActionImpl
: public IInputAction
{
public:
   CComPtr< IInputActionSite > m_pSite;

   HRESULT onLoad( LPTSTR szData )
   {
      return S_OK;
   }

   STDMETHOD(Initialize)(IInputActionSite *pSite, BSTR strData)
   {
      USES_CONVERSION;

      m_pSite = pSite;
      HRESULT hRes = static_cast< ImplT * >( this )->onLoad( OLE2T( strData ) );
      if( FAILED( hRes ) )
         m_pSite.Release();

      return hRes;
   }

   STDMETHOD(Terminate)()
   {
      m_pSite.Release();

      return S_OK;
   }

   // Default Implementation returns false
   STDMETHOD(get_Stackable)(VARIANT_BOOL *pVal)
   {
      *pVal = VARIANT_FALSE;
      return S_OK;
   }

   STDMETHOD(Push)()
   {
      return S_OK;
   }

   STDMETHOD(Pop)()
   {
      return S_OK;
   }

   STDMETHOD(Execute)()
   {
      return S_OK;
   }

   STDMETHOD(Reset)()
   {
      return S_OK;
   }
};

#endif
