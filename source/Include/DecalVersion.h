// DecalVersion.h
// Declaration of the Decal version resource

#ifndef __DECALVERSION_H__
#define __DECALVERSION_H__

#define		DECAL_MAJOR				2
#define		DECAL_MINOR				6
#define		DECAL_BUGFIX			1
#define		DECAL_RELEASE			1

#define		DECAL_VERSION_STRING		"2, 6, 1, 1"

#endif
