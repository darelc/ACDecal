; Script generated with the Venis Install Wizard

; Define your application name
!define APPNAME "Decal"
!define APPNAMEANDVERSION "Decal 2.6.1.0"

; Main Install settings
Name "${APPNAMEANDVERSION}"
InstallDir "$PROGRAMFILES\Decal"
InstallDirRegKey HKLM "Software\${APPNAME}" ""
OutFile "Decal2610.exe"

; Use compression
SetCompressor LZMA

; Modern interface settings
!include "MUI.nsh"

!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

; Set languages (first is default language)
!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_RESERVEFILE_LANGDLL

Section "" DecalCoreSection

	; Set Section properties
	SetOverwrite on

	; Set Section Files and Shortcuts
	SetOutPath "$INSTDIR\"

	File "Release\Switch-Active.bmp"
	File "Release\Switch-Inactive.bmp"
	File "Release\Switchbar Disabled.bmp"
	File "Release\Switchbar Template.bmp"
	File "Release\Tab-Active.bmp"
	File "Release\Tab-Inactive.bmp"

	File "Release\Decal.dll"
	File "Release\DecalControls.dll"
	File "Release\DecalDat.dll"
	File "Release\DecalFilters.dll"
	File "Release\DecalInput.dll"
	File "Release\DecalNet.dll"
	File "Release\DenAgent.exe"
	File "Release\ForceLibrary.dll"
	File "Release\Inject.dll"
	File "Release\LobbyHook.dll"
	File "Release\MapObject.bmp"
	File "Release\PlainText.dll"

	SetOutPath "$SYSDIR\"
	File "Release\LobbyHook.dll"

	RegDLL "$INSTDIR\Decal.dll"
  RegDLL "$INSTDIR\Inject.dll"
  RegDLL "$INSTDIR\DecalControls.dll"
  RegDLL "$INSTDIR\DecalDat.dll"
  RegDLL "$INSTDIR\DecalNet.dll"
  RegDLL "$INSTDIR\DecalFilters.dll"
  RegDLL "$INSTDIR\DecalInput.dll"
  RegDLL "$INSTDIR\PlainText.dll"
	
SectionEnd

Section "Desktop Shortcut" DesktopSection

	CreateShortCut "$DESKTOP\Decal Agent.lnk" "$INSTDIR\DenAgent.exe"

SectionEnd

Section "Start Menu Entry" SMSection

	CreateDirectory "$SMPROGRAMS\Decal"
	CreateShortCut "$SMPROGRAMS\Decal\Decal Agent.lnk" "$INSTDIR\DenAgent.exe"
	CreateShortCut "$SMPROGRAMS\Decal\Uninstall.lnk" "$INSTDIR\uninstall.exe"

SectionEnd

Section "Quick Launch Shortcut" QLSection

	CreateShortCut "$QUICKLAUNCH\Decal Agent.lnk" "$INSTDIR\DenAgent.exe"

SectionEnd

Section -FinishSection

	WriteRegStr HKLM "Software\${APPNAME}" "" "$INSTDIR"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "DisplayName" "${APPNAME}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}" "UninstallString" "$INSTDIR\uninstall.exe"
	WriteUninstaller "$INSTDIR\uninstall.exe"

SectionEnd

; Modern install component descriptions
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${Section1} ""
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;Uninstall section
Section Uninstall

	;Remove from registry...
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APPNAME}"
	DeleteRegKey HKLM "SOFTWARE\${APPNAME}"

	; Delete self
	Delete "$INSTDIR\uninstall.exe"

	; Delete Shortcuts
	Delete "$DESKTOP\Decal.lnk"
	Delete "$SMPROGRAMS\Decal\Decal.lnk"
	Delete "$SMPROGRAMS\Decal\Uninstall.lnk"
	
	; Unregister DLLS
  UnRegDLL "$INSTDIR\Decal.dll"
  UnRegDLL "$INSTDIR\Inject.dll"
  UnRegDLL "$INSTDIR\DecalControls.dll"
  UnRegDLL "$INSTDIR\DecalDat.dll"
  UnRegDLL "$INSTDIR\DecalNet.dll"
  UnRegDLL "$INSTDIR\DecalFilters.dll"
  UnRegDLL "$INSTDIR\DecalInput.dll"
  UnRegDLL "$INSTDIR\PlainText.dll"

	; Clean up Decal
	Delete "$INSTDIR\Decal.dll"
	Delete "$INSTDIR\DecalControls.dll"
	Delete "$INSTDIR\DecalDat.dll"
	Delete "$INSTDIR\DecalFilters.dll"
	Delete "$INSTDIR\DecalInput.dll"
	Delete "$INSTDIR\DecalNet.dll"
	Delete "$INSTDIR\DenAgent.exe"
	Delete "$INSTDIR\ForceLibrary.dll"
	Delete "$INSTDIR\Inject.dll"
	Delete "$INSTDIR\LobbyHook.dll"
	Delete "$INSTDIR\MapObject.bmp"
	Delete "$INSTDIR\PlainText.dll"
	Delete "$INSTDIR\Switch-Active.bmp"
	Delete "$INSTDIR\Switch-Inactive.bmp"
	Delete "$INSTDIR\Switchbar Disabled.bmp"
	Delete "$INSTDIR\Switchbar Template.bmp"
	Delete "$INSTDIR\Tab-Active.bmp"
	Delete "$INSTDIR\Tab-Inactive.bmp"

	; Remove remaining directories
	RMDir "$SMPROGRAMS\Decal"
	RMDir "$INSTDIR\"

SectionEnd

; eof