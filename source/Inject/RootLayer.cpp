// RootLayer.cpp : Implementation of cRootLayer
#include "stdafx.h"
#include "Inject.h"
#include "RootLayer.h"
#include "Manager.h"
#include "BarLayer.h"
#include "Panel.h"
#include "View.h"

enum eChildIDs
{
   eChildBars,
   eChildPanel
};

/////////////////////////////////////////////////////////////////////////////
// cRootLayer

void cRootLayer::addView( cView *pView, ILayer *pRoot )
{
   m_views.push_back( pView );

   // Next, make the bar entry
   m_pBars->AddBar( pView->m_nViewID, &pView->m_VP );

   // Last, make the view entry
   pView->m_pPanel->AddView( pView->m_nViewID, pRoot );
}

void cRootLayer::removeView( cView *pView )
{
   for( cViewList::iterator i = m_views.begin(); i != m_views.end(); ++ i )
   {
      if( *i == pView )
      {
         m_views.erase( i );
         break;
      }
   }
}

void cRootLayer::onCreate()
{
   LayerParams lpBars = { eChildBars, { 0, 0, 1, 1 }, 0 };

   CComObject< cBarLayer > *pBars;
   CComObject< cBarLayer >::CreateInstance( &pBars );

   m_pBars = pBars;

   m_pSite->CreateChild( &lpBars, pBars );

   if( m_nViewMode == 0 ) // Single Mode
   {
		LayerParams lpPanel = { eChildPanel, { 0, 0, 0, 0 }, 0 };

		CComObject< cPanel > *pPanel;
		CComObject< cPanel >::CreateInstance( &pPanel );

		m_pSite->CreateChild( &lpPanel, pPanel );
		pPanel->putref_Sink( this );

		m_pPanel = pPanel;
   }
}

void cRootLayer::onDestroy()
{
   m_pBars.Release();
}

STDMETHODIMP cRootLayer::CreateView(ViewParams *pParams, ILayer *pLayer, IView **ppView)
{  
   // First create the view object
   CComObject< cView > *pView;
   CComObject< cView >::CreateInstance( &pView );

   if( m_nViewMode == 1 ) // Multi Mode
	{
		LayerParams lpPanel = { m_nNextViewID, { 0, 0, 0, 0 }, 0 };

		CComObject< cPanel > *pPanel;
		CComObject< cPanel >::CreateInstance( &pPanel );

		m_pSite->CreateChild( &lpPanel, pPanel );

		pPanel->putref_Sink( this );
		pView->m_pPanel = pPanel;
	}
	else
		pView->m_pPanel = m_pPanel;

   pView->m_pRoot = this;
   pView->m_nViewID = m_nNextViewID ++;
   pView->m_VP.label = _bstr_t(pParams->label).copy();
   pView->m_VP.icon = pParams->icon;
   pView->m_VP.iconLibrary = pParams->iconLibrary;
   pView->m_VP.left = pParams->left;
   pView->m_VP.top = pParams->top;
   pView->m_VP.width = pParams->width;
   pView->m_VP.height = pParams->height;

   addView( pView, pLayer );

   *ppView = pView;
   pView->AddRef();

	return S_OK;
}

STDMETHODIMP cRootLayer::SelectBar(long nID)
{
// We are given the Bar ID, find the matching view
	for( cViewList::iterator i = m_views.begin(); i != m_views.end(); ++ i )
	{
		if( ( *i )->m_nViewID == nID )
		{
			long nPrevActive;
			( *i )->m_pPanel->get_ActiveView( &nPrevActive );

			if( nPrevActive != nID )
			{
				( *i )->m_pPanel->ActivateView( ( *i )->m_nViewID, & (*i)->m_VP, (long*)(*i));
			
				( *i )->Fire_Activate();
				( *i )->m_bActivated = true;
			}
			else
			{
				( *i )->Deactivate();
				( *i )->m_bActivated = false;
			}

			return S_OK;
		}
	}

	// Invalid return value
	return E_INVALIDARG;
}

STDMETHODIMP cRootLayer::LoadView(BSTR strXML, IView **ppView)
{   
   CComObject< cView > *pView;
   CComObject< cView >::CreateInstance( &pView );

	if( m_nViewMode == 1 ) // Multi Mode
		{
			LayerParams lpPanel = { m_nNextViewID, { 0, 0, 0, 0 }, 0 };

			CComObject< cPanel > *pPanel;
			CComObject< cPanel >::CreateInstance( &pPanel );

			m_pSite->CreateChild( &lpPanel, pPanel );

			pPanel->putref_Sink( this );
			pView->m_pPanel = pPanel;
			pPanel->put_Params( &pView->m_VP );
		}
		else
		{
			pView->m_pPanel = m_pPanel;
			m_pPanel->put_Params( &pView->m_VP );
		}

	CComPtr< IUnknown > pRootControl;

	pView->m_pRoot = this;
	pView->m_nViewID = m_nNextViewID ++;
	
	long lViewFlags = pView->loadSchema( strXML, &pRootControl );

	if( pRootControl.p )
	{
		m_views.push_back( pView );

		*ppView = pView;
		pView->AddRef();

		// Next, make the bar entry
		m_pBars->AddBar( pView->m_nViewID, &pView->m_VP );

		// Last, create all the controls
		pView->m_pPanel->LoadViewEx( pView->m_nViewID, pView, pRootControl, lViewFlags );

		if( cManager::_p->m_bXMLViewViewer )
		{
			pView->m_VP.top = 24;
			pView->Activate( );
		}
	}

	else
	{
		pView->Release( );
		*ppView = NULL;
		return E_FAIL;
	}
	return S_OK;
}

STDMETHODIMP cRootLayer::PanelDeactivate(long nViewID)
{
   // Find the view of our desires and fire it's hide message
   for( cViewList::iterator i = m_views.begin(); i != m_views.end(); ++ i )
   {
      if( ( *i )->m_nViewID == nViewID )
         ( *i )->Fire_Deactivate(),( *i )->m_bActivated = false;
   }

   return S_OK;
}

STDMETHODIMP cRootLayer::LoadViewObject(IUnknown *pSchema, IView **ppView)
{
   CComObject< cView > *pView;
   CComObject< cView >::CreateInstance( &pView );

   if( m_nViewMode == 1 ) // Multi Mode
	{
		LayerParams lpPanel = { m_nNextViewID, { 0, 0, 0, 0 }, 0 };

		CComObject< cPanel > *pPanel;
		CComObject< cPanel >::CreateInstance( &pPanel );

		m_pSite->CreateChild( &lpPanel, pPanel );

		pPanel->putref_Sink( this );
		pView->m_pPanel = pPanel;
	}
	else
		pView->m_pPanel = m_pPanel;

   CComPtr< IUnknown > pRootControl;

   pView->m_pRoot = this;
   pView->m_nViewID = m_nNextViewID ++;
   pView->loadSchemaObject( pSchema, &pRootControl );
  
   m_views.push_back( pView );

   *ppView = pView;
   pView->AddRef();

   // Next, make the bar entry
   m_pBars->AddBar( pView->m_nViewID, &pView->m_VP );

   // Last, create all the controls
   pView->m_pPanel->LoadView( pView->m_nViewID, pView, pRootControl );
	return S_OK;
}