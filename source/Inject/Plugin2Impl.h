// Plugin2Impl.h
//
// Declaration of helper class for implementing type 2 plugins

#ifndef _PLUGIN2IMPL_H_
#define _PLUGIN2IMPL_H_

template<class cImpl> class IPlugin2Impl : public IPlugin2
{
public:
	CComPtr<IPluginSite>  m_pSite;
	CComPtr<IPluginSite2> m_pSite2;
	CComPtr<IDecal>       m_pDecal;

	virtual HRESULT onInitialize() = 0;
	virtual HRESULT onTerminate() = 0;

	STDMETHOD(Initialize)(IPluginSite2 *pSite2)
	{
		HRESULT hRes;

		// Store the Type 2 (Decal) Plugin Site
		m_pSite2 = pSite2;

		// Get Decal from Type 2 Plugin Site
		hRes = m_pSite2->get_Decal(&m_pDecal);
		if (!SUCCEEDED(hRes))
			return hRes;

		// Get Type 1 (Inject) Plugin Site
		hRes = m_pDecal->get_Object(_bstr_t("services\\DecalPlugins.InjectService\\site"), 
			__uuidof(IPluginSite), reinterpret_cast<LPVOID *> (&m_pSite));
		if (!SUCCEEDED(hRes))
			return hRes;

		// Initialize things
		hRes = static_cast<cImpl *>(this)->onInitialize();

		return hRes;
	}

	STDMETHOD(Terminate)()
	{
		HRESULT hRes;

		// Terminate the works
		hRes = static_cast<cImpl *>(this)->onTerminate();

		// Release the elements
		m_pSite.Release();
		m_pDecal.Release();
		m_pSite2.Release();

		return hRes;
	}
};

#endif // _PLUGIN2IMPL_H_
