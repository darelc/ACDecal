// Direct3DHook.cpp : Implementation of CDirect3DHook
#include "stdafx.h"
#include "Inject.h"
#include "Direct3DHook.h"

#include "InjectApi.h"
#include "Manager.h"

/////////////////////////////////////////////////////////////////////////////
// CDirect3DHook

void CDirect3DHook::setObject( IUnknown *pDevice )
{
	pDevice->QueryInterface( IID_IDirect3DDevice, reinterpret_cast< void ** >( &m_pDevice ) );
	pDevice->QueryInterface( IID_IDirect3DDevice2, reinterpret_cast< void ** >( &m_pDevice2 ) );
	pDevice->QueryInterface( IID_IDirect3DDevice3, reinterpret_cast< void ** >( &m_pDevice3 ) );
}

STDMETHODIMP CDirect3DHook::BeginScene()
{
	cManager::_p->sendPreBeginScene();

	HRESULT hRes = m_pDevice->BeginScene();

	cManager::_p->sendPostBeginScene();

	return hRes;
}

STDMETHODIMP CDirect3DHook::EndScene()
{
	cManager::_p->draw3D();
	cManager::_p->sendPreEndScene();

	HRESULT hRes = m_pDevice->EndScene();

	// Draw the user 2D layer
	cManager::_p->draw2D();
	cManager::_p->sendPostEndScene();

	return hRes;
}

