// stdafx.h : include file for standard system include files,
//      or project specific include files that are used frequently,
//      but are changed infrequently

#if !defined(AFX_STDAFX_H__F572112C_DAE7_4CC4_9843_056AF6606BEF__INCLUDED_)
#define AFX_STDAFX_H__F572112C_DAE7_4CC4_9843_056AF6606BEF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define STRICT
#define _WIN32_WINDOWS 0x0410
#define _WIN32_DCOM

#define _ATL_APARTMENT_THREADED
#define D3D_OVERLOADS
#define DIRECTINPUT_VERSION 0x600

#include <atlbase.h>
//You may derive a class from CComModule and use it if you want to override
//something, but do not change the name of _Module
extern CComModule _Module;
typedef UINT (CALLBACK* LPFNDLLFUNCALPHABLEND)(HDC, int, int, int, int, HDC, int, int, int, int, BLENDFUNCTION);
extern LPFNDLLFUNCALPHABLEND AlphaBlendF;
#include <atlcom.h>

#include <ddraw.h>
#include <d3d.h>
#include <winable.h>

#include <list>
#include <map>
#include <vector>
#include <stack>
#include <deque>
#include <queue>
#include <set>
#include <string>
#import <msxml.dll>
#include "../Include/Helpers.h"
#include "../Include/VSBridge.h"
#define MakePtr( cast, ptr, AddValue ) (cast)( (DWORD)(ptr)+(DWORD)(AddValue))

#ifdef _CHECKMEM
#define _ASSERTMEM(a) _ASSERTE(a)
#else
#define _ASSERTMEM(a)
#endif

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__F572112C_DAE7_4CC4_9843_056AF6606BEF__INCLUDED)
