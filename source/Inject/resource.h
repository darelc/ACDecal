//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Inject.rc
//
#define IDS_PROJNAME                    100
#define IDR_DIRECTDRAWHOOK              101
#define IDR_DIRECT3DHOOK                102
#define IDR_MATERIALHOOK                103
#define IDR_DIRECTINPUTHOOK             104
#define IDR_GENINDEXDEVICEHOOK          105
#define IDR_PLUGINADAPTERV1             110
#define IDR_SCROLLER                    120
#define IDR_LIST                        121
#define IDR_TEXTCOLUMN                  122
#define IDR_ICONCOLUMN                  123
#define IDR_CHECKCOLUMN                 124
#define IDR_LISTVIEW                    125
#define IDR_NOTEBOOK                    126
#define IDR_MAINFRAME                   128
#define IDR_ECHOFILTER                  129
#define IDR_SOLIDIMAGE                  130
#define IDR_INPUTBUFFER                 131
#define IDR_INJECTSERVICE               134
#define IDR_XMLVIEWVIEWER               135
#define IDR_BUTTON                      202
#define IDR_PAGER                       203

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        207
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           136
#endif
#endif
