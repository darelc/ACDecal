// DirectDrawSurfaceHook.cpp : Implementation of CDirectDrawSurfaceHook
#include "stdafx.h"
#include "Inject.h"
#include "DirectDrawSurfaceHook.h"

#include "InjectApi.h"
#include "Manager.h"

void CDirectDrawSurfaceHook::setObject( IUnknown *pDevice )
{
   pDevice->QueryInterface( IID_IDirectDrawSurface, reinterpret_cast< void ** >( &m_pDDS ) );
   pDevice->QueryInterface( IID_IDirectDrawSurface2, reinterpret_cast< void ** >( &m_pDDS2 ) );
   pDevice->QueryInterface( IID_IDirectDrawSurface3, reinterpret_cast< void ** >( &m_pDDS3 ) );
   pDevice->QueryInterface( IID_IDirectDrawSurface4, reinterpret_cast< void ** >( &m_pDDS4 ) );
}

void CDirectDrawSurfaceHook::setSurfaceNum( long nSurfaceNum )
{
	m_nSurfaceNum = nSurfaceNum;
}

STDMETHODIMP CDirectDrawSurfaceHook::Blt(LPRECT p1,LPDIRECTDRAWSURFACE4 p2, LPRECT p3,DWORD p4, LPDDBLTFX p5)
{
//	MessageBeep(0);
	//	MessageBox(0, "I AM FROM DIRECTDRAWSURFACEHOOK BLT", "YAY!", 0);
 //  cManager::_p->draw2D();

 /*  DDSURFACEDESC2 ddsd;
   ddsd.dwSize = sizeof( DDSURFACEDESC2 );
   m_pDDS4->GetSurfaceDesc( &ddsd );
*/
/*	HANDLE hFile;
hFile = CreateFile("c:\\acpoo72.txt", GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_DELETE | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
SetFilePointer(hFile, 0, 0, FILE_END);
DWORD lpbw;
char tmp[1024];
//if(p1 != NULL && p3 != NULL)
if(p5==NULL)
wsprintf(tmp, "%d left:%d top:%d right:%d bottom:%d -> left:%d top:%d right:%d bottom:%d\r\n", m_pDDS4, p1->left, p1->top, p1->right, p1->bottom, p3->left, p3->top, p3->right, p3->bottom);

WriteFile(hFile, tmp, strlen(tmp), &lpbw, NULL);
//WriteFile(hFile, (char*)(_bstr_t((long)ddsd.dwWidth) + _bstr_t("\r\n")), _bstr_t((long)ddsd.dwWidth).length() + 2, &lpbw, NULL);
CloseHandle(hFile);
*//*
if(p3!=NULL)
   if(p3->right-p3->left==640)
	   MessageBeep(0);
*/

/*	DDBLTFX       ddbltfx;

    ddbltfx.dwSize=sizeof(ddbltfx);
    ddbltfx.dwFillColor=2;

    HRESULT hRes = m_pDDS4->Blt(NULL,NULL,NULL,DDBLT_COLORFILL,&ddbltfx);*/

/*	if(p3!=NULL)
		if((p3->right-p3->left)==332)
			MessageBox(0, _bstr_t(m_nSurfaceNum), "asd", 0);
*/
//	MessageBeep(0);
   //HRESULT hRes = m_pDDS4->Blt(p1, p2, p3, p4, p5);
   
//   cManager::_p->draw2D();
//	MessageBeep(0);
	return m_pDDS4->Blt(p1, p2, p3, p4, p5);
//	return 0;//hRes;
}

STDMETHODIMP CDirectDrawSurfaceHook::Flip( LPDIRECTDRAWSURFACE4 p1, DWORD p2) 
{
/*
   DDSURFACEDESC2 ddsd;
   ddsd.dwSize = sizeof( DDSURFACEDESC2 );
   m_pDDS4->GetSurfaceDesc( &ddsd );

HANDLE hFile;
hFile = CreateFile("c:\\acpoo4.txt", GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_DELETE | FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
SetFilePointer(hFile, 0, 0, FILE_END);
DWORD lpbw;
WriteFile(hFile, (char*)(_bstr_t((long)ddsd.dwWidth) + _bstr_t("\r\n")), _bstr_t((long)ddsd.dwWidth).length() + 2, &lpbw, NULL);
CloseHandle(hFile);*/

	return m_pDDS4->Flip(p1, p2);
}
