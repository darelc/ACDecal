// InjectApi.h
// Declaration of global injection functions
#include <ddraw.h>
#include <d3d.h>

#ifndef __INJECTAPI_H
#define __INJECTAPI_H

#ifdef INJECT_IMPL
#define INJECT_API   __declspec(dllexport)
#else
#define INJECT_API   __declspec(dllimport)
#endif

#include "Inject.h"

// Enumerations
enum eInjectPath
{
   eInjectPathDatFile,
   eInjectPathAgent
};

// Exported functions from Inject.dll
// Adds a reference to the registered hook function
void INJECT_API InjectEnable();

// Removes a reference to the registered hook function
void INJECT_API InjectDisable();

// Prepends a path to your filename, returns szBuffer
LPTSTR INJECT_API InjectMapPath( eInjectPath pathType, LPCTSTR szFilename, LPTSTR szBuffer );

// Checksum - Added for my test container
void INJECT_API Container_Initialize( HWND hWnd, IDirectDraw4* pDD4, IDirectDrawSurface4 *pDDS4 );
void INJECT_API Container_StartPlugins( );
void INJECT_API Container_StopPlugins( );
void INJECT_API Container_Terminate( );
void INJECT_API Container_SetSurface( IDirectDrawSurface4 *pDDS4 );
void INJECT_API Container_Draw( );

void INJECT_API XMLViewer_Initialize( HWND hWnd, IDirectDraw4* pDD4, IDirectDrawSurface4 *pDDS4 );
void INJECT_API XMLViewer_LoadView( BSTR bstrSchema );
void INJECT_API XMLViewer_RemoveView( IView* pView  );
void INJECT_API XMLViewer_Terminate( );
void INJECT_API XMLViewer_Draw( );
#endif
