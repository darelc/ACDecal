// SimpleBar.h : Declaration of the cSimpleBar

#ifndef __SIMPLEBAR_H_
#define __SIMPLEBAR_H_

#include "resource.h"		// main symbols

#include "SinkImpl.h"

/////////////////////////////////////////////////////////////////////////////
// cSimpleBar
class ATL_NO_VTABLE cSimpleBar : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public ISimpleBar,
	public cNoEventsImpl,
	public ILayerImpl< cSimpleBar >,
	public ILayerRenderImpl,
	public ILayerMouseImpl
{
public:
	cSimpleBar()
		: m_bSelected( VARIANT_FALSE )
	{
	}

	_bstr_t m_strLabel;
	long m_nIconModule;
	long m_nIconID;
	long m_nMinMax;

	VARIANT_BOOL m_bSelected;

	CComPtr< IImageCache > m_pSwitch;
	CComPtr< IImageCache > m_pSwitchDisabled;
	CComPtr< IFontCache > m_pFont;

	void onCreate();
	void onDestroy();

BEGIN_COM_MAP(cSimpleBar)
	COM_INTERFACE_ENTRY(ILayerRender)
	COM_INTERFACE_ENTRY(ILayerMouse)
	COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(ISimpleBar)
END_COM_MAP()

// ISimpleBar
public:
	// ISimpleBar Functions
	STDMETHOD(get_Params)(/*[out, retval]*/ ViewParams *pVal);
	STDMETHOD(put_Params)(/*[in]*/ ViewParams * newVal);
	STDMETHOD(get_RenderWidth)(long *nWidth);

	// ILayerRender Functions
	STDMETHOD(Render)(ICanvas *pCanvas);

	// ILayerMouse Functions
	STDMETHOD(MouseEnter)(MouseState *);
	STDMETHOD(MouseExit)(MouseState *);
	STDMETHOD(MouseDown)(MouseState *);
};

#endif //__SIMPLEBAR_H_
