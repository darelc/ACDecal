// Button.h : Declaration of the cButton

#ifndef __BUTTON_H_
#define __BUTTON_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "InjectCP.h"

/////////////////////////////////////////////////////////////////////////////
// cButton
class ATL_NO_VTABLE cButton : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public CComCoClass<cButton, &CLSID_Button>,
	public IControlImpl< cButton, IButton, &IID_IButton, &LIBID_DecalPlugins >,
   public ILayerImpl< cButton >,
   public ILayerRenderImpl,
   public ILayerMouseImpl,
   public ILayerSchema,
   public IProvideClassInfo2Impl< &CLSID_Button, &DIID_ICommandEvents, &LIBID_DecalPlugins >,
   public IConnectionPointContainerImpl<cButton>,
   public CProxyICommandEvents< cButton >
{
public:
	cButton();

   long m_nModule;
   long m_nReleased;
   long m_nPressed;
   long m_nBackground;

   VARIANT_BOOL m_bPressed,
      m_bMouseIn;

   long m_nMatte;

DECLARE_REGISTRY_RESOURCEID(IDR_BUTTON)
DECLARE_PROTECT_FINAL_CONSTRUCT()

public :
BEGIN_COM_MAP(cButton)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayerMouse)
   COM_INTERFACE_ENTRY(ILayer)
   COM_INTERFACE_ENTRY(ILayerSchema)
	COM_INTERFACE_ENTRY(IButton)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cButton)
CONNECTION_POINT_ENTRY(DIID_ICommandEvents)
END_CONNECTION_POINT_MAP()

// IButton
public:
   // IButton Methods
	STDMETHOD(SetImages)(long dwModule, long dwReleased, long dwPressed);
	STDMETHOD(get_Matte)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Matte)(/*[in]*/ long newVal);

   // ILayerRender Methods
	STDMETHOD(Render)(ICanvas *pCanvas);

   // ILayerMouse Methods
	STDMETHOD(MouseEnter)(struct MouseState *);
	STDMETHOD(MouseExit)(struct MouseState *);
	STDMETHOD(MouseDown)(struct MouseState *);
	STDMETHOD(MouseUp)(struct MouseState *);

   // ILayerSchema Methods
   STDMETHOD(SchemaLoad)(IView *, IUnknown *pSchema);
};

#endif //__BUTTON_H_
