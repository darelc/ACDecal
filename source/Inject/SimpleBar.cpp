// SimpleBar.cpp : Implementation of cSimpleBar
#include "stdafx.h"
#include "Inject.h"
#include "SimpleBar.h"

/////////////////////////////////////////////////////////////////////////////
// cSimpleBar

void cSimpleBar::onCreate()
{
	CComPtr< IPluginSite > pPlugin;
	m_pSite->get_PluginSite( &pPlugin );

	pPlugin->LoadBitmapFile( _bstr_t( _T( "Switch-Active.bmp" ) ), &m_pSwitch );
	pPlugin->LoadBitmapFile( _bstr_t( _T( "Switch-Inactive.bmp" ) ), &m_pSwitchDisabled );
	BSTR bstrFontName;
	pPlugin->get_FontName(&bstrFontName);
	pPlugin->CreateFont( bstrFontName /*_bstr_t( _T( "Times New Roman" ) )*/, 14, 0, &m_pFont );
	m_nMinMax = 0;

	m_pSite->put_Transparent( VARIANT_FALSE );
}

void cSimpleBar::onDestroy()
{
	m_pSwitch.Release();
	m_pSwitchDisabled.Release();
	m_pFont.Release();
}

#define SWITCH_STRETCH_START 25
#define SWITCH_STRETCH_END 55

#define TEXT_ICON_GAP 4
#define END_GAP 8

#define ICON_TOP 2
#define TEXT_TOP 2

#define ICON_SIZE 16

STDMETHODIMP cSimpleBar::Render(ICanvas *pCanvas)
{
	RECT rc;
	m_pSite->get_Position( &rc );

	int nWidth = rc.right - rc.left;

	// First draw the background image
	static POINT ptOrg = { 0, 0 };
	if( m_bSelected )
		m_pSwitch->StretchBlt( pCanvas, &ptOrg, nWidth, SWITCH_STRETCH_START, SWITCH_STRETCH_END );
	else
		m_pSwitchDisabled->StretchBlt( pCanvas, &ptOrg, nWidth, SWITCH_STRETCH_START, SWITCH_STRETCH_END );

	// Next the icon
	CComPtr< IPluginSite > pPlugin;
	m_pSite->get_PluginSite( &pPlugin );

	CComPtr< IIconCache > pIcons;
	static SIZE szIcon = { ICON_SIZE, ICON_SIZE };
	pPlugin->GetIconCache( &szIcon, &pIcons );

	static POINT ptIcon = { 0, ICON_TOP };
	pIcons->DrawIcon( &ptIcon, m_nIconID, m_nIconModule, pCanvas );

	if (m_nMinMax == eStateMax)
	{
		static POINT ptText = { ICON_SIZE + TEXT_ICON_GAP, TEXT_TOP };
		m_pFont->DrawText( &ptText, m_strLabel, ( m_bSelected ) ? RGB( 0, 0, 0 ) : RGB( 255, 255, 255 ), pCanvas );
	}

	_ASSERTMEM( _CrtCheckMemory( ) );

	return S_OK;
}

STDMETHODIMP cSimpleBar::MouseEnter(MouseState *)
{
	m_bSelected = VARIANT_TRUE;
	m_pSite->Invalidate();

	return S_OK;
}

STDMETHODIMP cSimpleBar::MouseExit(MouseState *)
{
	m_bSelected = VARIANT_FALSE;
	m_pSite->Invalidate();

	return S_OK;
}

STDMETHODIMP cSimpleBar::MouseDown(MouseState *)
{
	CComPtr< IRootLayer > pRoot;

	m_pSite->GetParentSink( IID_IRootLayer, reinterpret_cast< void ** >( &pRoot ) );

	_ASSERTE( pRoot.p != NULL );

	long nID;
	m_pSite->get_ID( &nID );
	pRoot->SelectBar( nID );

	return S_OK;
}

STDMETHODIMP cSimpleBar::get_RenderWidth(long *nWidth)
{
	if (m_nMinMax == eStateMin)
	{
		*nWidth = 20; 
	}
	else
	{
	  SIZE sz;
		m_pFont->MeasureText( m_strLabel, &sz );

		// Add in the requested size to the parent
		*nWidth = sz.cx + TEXT_ICON_GAP + END_GAP + ICON_SIZE;
	}
	return S_OK;
}

STDMETHODIMP cSimpleBar::get_Params(ViewParams *pVal)
{
	USES_CONVERSION;

	pVal->icon = m_nIconID;
	pVal->iconLibrary = m_nIconModule;
	pVal->label = OLE2BSTR( m_strLabel );
	pVal->state = m_nMinMax;

	return S_OK;
}

STDMETHODIMP cSimpleBar::put_Params(ViewParams *newVal)
{
	m_nIconID = newVal->icon;
	m_nIconModule = newVal->iconLibrary;
	m_strLabel = newVal->label;
	m_nMinMax = newVal->state;

	return S_OK;
}