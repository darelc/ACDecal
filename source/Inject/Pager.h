// Pager.h : Declaration of the cPager

#ifndef __PAGER_H_
#define __PAGER_H_

#include "resource.h"       // main symbols

#include "SinkImpl.h"
#include "InjectCP.h"

/////////////////////////////////////////////////////////////////////////////
// cPager
class ATL_NO_VTABLE cPager : 
	public CComObjectRootEx<CComMultiThreadModel>,
   public CComCoClass<cPager, &CLSID_Pager>,
   public ILayerImpl< cPager >,
   public ILayerRenderImpl,
   public IControlImpl< cPager, IPager, &IID_IControl, &LIBID_DecalPlugins >,
   public IProvideClassInfo2Impl< &CLSID_Pager, &DIID_IPagerEvents, &LIBID_DecalPlugins >,
   public IConnectionPointContainerImpl<cPager>,
   public CProxyIPagerEvents< cPager >
{
public:
	cPager()
	{
	}

   POINT m_ptScrollFrom;
   DWORD m_dwTimeStart;

   POINT m_ptScrollTo;
   DWORD m_dwTimeEnd;

   POINT m_ptCurrent;

   long m_nCommand;
   VARIANT_BOOL m_bContinue, m_bScrolling;

   void onCreate();

DECLARE_REGISTRY_RESOURCEID(IDR_PAGER)
DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cPager)
   COM_INTERFACE_ENTRY(ILayerRender)
   COM_INTERFACE_ENTRY(ILayer)
	COM_INTERFACE_ENTRY(IPager)
   COM_INTERFACE_ENTRY(IControl)
   COM_INTERFACE_ENTRY(IDispatch)
   COM_INTERFACE_ENTRY(IProvideClassInfo)
   COM_INTERFACE_ENTRY(IProvideClassInfo2)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()

BEGIN_CONNECTION_POINT_MAP(cPager)
CONNECTION_POINT_ENTRY(DIID_IPagerEvents)
END_CONNECTION_POINT_MAP()

   void setupScroll( long dwCommand, LPPOINT pptDest );
   void updatePosition();
   void endScroll();

// IPager
public:
   // IPager Methods
	STDMETHOD(get_Command)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Command)(/*[in]*/ long newVal);
	STDMETHOD(ScrollTo)(LPPOINT pVal);
	STDMETHOD(FinishCommand)();
	STDMETHOD(get_Offset)(/*[out, retval]*/ LPPOINT pVal);
	STDMETHOD(put_Offset)(/*[in]*/ LPPOINT newVal);
	STDMETHOD(CreateClient)(ILayer *pLayer);

   // ILayerRenderMethods
   STDMETHOD(PreRender)();
   STDMETHOD(AdjustRenderArea)( ICanvas *pCanvas, VARIANT_BOOL *pbDrawChildren );
};

#endif //__PAGER_H_
