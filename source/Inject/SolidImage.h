// SolidImage.h : Declaration of the cSolidImage

#ifndef __SOLIDIMAGE_H_
#define __SOLIDIMAGE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cSolidImage
class ATL_NO_VTABLE cBrushImage : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IBrushImage,
	public IDispatchImpl< IImageCache, &IID_IImageCacheDisp, &LIBID_DecalPlugins >
{
public:
	cBrushImage()
      : m_nColor( RGB( 0, 255, 255 ) )
	{
	}

   long m_nColor;

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(cBrushImage)
	COM_INTERFACE_ENTRY(IBrushImage)
	COM_INTERFACE_ENTRY(IImageCache)
   COM_INTERFACE_ENTRY(IImageCacheDisp)
   COM_INTERFACE_ENTRY(IDispatch)
END_COM_MAP()

public:
   // IBrushImage Methods
	STDMETHOD(get_Color)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_Color)(/*[in]*/ long newVal);

   // IImageCache Methods
	STDMETHOD(PatBlt)(ICanvas * pDest, tagRECT * prcDest, tagPOINT * ptOrigin)
	{
      if( m_nColor != RGB( 0, 255, 255 ) )
         pDest->Fill( prcDest, m_nColor );

		return S_OK;
	}

	STDMETHOD(StretchBlt)(ICanvas * pDest, tagPOINT * pptDest, LONG nWidth, LONG nStartStretch, LONG nEndStretch)
	{
      // Not implemented, not a real image
      _ASSERTE( FALSE );
		return E_NOTIMPL;
	}

	STDMETHOD(Blt)(tagRECT * rcSrc, ICanvas * pDest, tagPOINT * pptDest)
	{
      // Not implemented, not a real image
      _ASSERTE( FALSE );
		return E_NOTIMPL;
	}

	STDMETHOD(get_Size)(tagSIZE * pVal)
	{
      _ASSERTE( pVal != NULL );
		if (pVal == NULL)
			return E_POINTER;
			
      // Not implemented, not a real image
      _ASSERTE( FALSE );
		return E_NOTIMPL;
	}

	STDMETHOD(StretchBltArea)(tagRECT * prcSrc, ICanvas * pDest, tagRECT * prcDest)
	{
      // Not implemented, not a real image
      _ASSERTE( FALSE );
		return E_NOTIMPL;
	}
};

#endif //__SOLIDIMAGE_H_
