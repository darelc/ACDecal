// IconCache.h : Declaration of the cIconCache

#ifndef __ICONCACHE_H_
#define __ICONCACHE_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// cIconCache
class ATL_NO_VTABLE cIconCache : 
	public CComObjectRootEx<CComMultiThreadModel>,
	public IIconCache
{
public:
	cIconCache()
	{
	}

   SIZE m_szIcon;
   long m_nEdge;

   struct cIconID
   {
      HMODULE m_hMod;
      DWORD m_dwID;
	  long m_lBColor;	//Moputu - 05172002: Added to make icons with different colored bordered unique.
   };

   typedef std::vector< cIconID > cIDList;

   class cIconBuffer
   {
   public:
      cIDList m_icons;
      CComPtr< ICanvas > m_pSurface;
   };

   typedef std::list< cIconBuffer > cIconBufferList;
   cIconBufferList m_icons;

   bool findIcon( HMODULE hMod, DWORD dwFile, cIconBuffer *&pBuffer, int &nIndex, long lColor = -1  ); //Moputu - 05172002: Added long lColor = -1 to make icons with different colored borders unique.
   void findFreeSlot( cIconBuffer *&pBuffer, int &nIndex );

   bool loadIcon( DWORD dwFile, cIconBuffer *&pBuffer, int &nIndex, long lColor = -1 );
   bool loadIconResource( HMODULE hMod, DWORD dwResourceID, cIconBuffer *&pBuffer, int &nIndex, long lColor = -1  );  //Moputu - 05172002: Added long lColor = -1 to make icons with different colored borders unique.

   void drawIcon( LPPOINT pPos, ICanvas *pDest, cIconBuffer *pBuffer, int nIndex );

BEGIN_COM_MAP(cIconCache)
	COM_INTERFACE_ENTRY(IIconCache)
END_COM_MAP()

// IIconCache
public:
	STDMETHOD(DrawIcon)( LPPOINT ppt, long nFile, long nModule, ICanvas *pTarget );
	STDMETHOD(DrawIconEx)( LPPOINT ppt, long nFile, long nModule, ICanvas *pTarget, long lColor );
};

#endif //__ICONCACHE_H_
